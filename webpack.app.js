'use strict';

const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const baseConfig = require('./webpack.config.js');

const environment = process.env.NODE_ENV || 'development';
const isProduction = environment === 'production';
const botWidgetUrl = isProduction ? process.env.BOT_WIDGET_URL : 'http://flowxo-dev.cc:9010';
const webMessengerShortUrl = isProduction ? process.env.WEB_MESSENGER_SHORT_URL : 'http://flowxo-dev.cc:9040';

module.exports = webpackMerge(baseConfig, {
  entry: {
    app: [path.resolve(__dirname, 'client', 'scripts', 'app', 'index.js')],
  },
  resolve: {
    alias: {
      scripts: path.resolve(__dirname, 'client', 'scripts'),
      views: path.resolve(__dirname, 'client', 'views')
    }
  },
  module: {
    rules: [{
      test: /\.js$/,
      include: [
        path.resolve(__dirname, 'client', 'scripts'),
        path.resolve(__dirname, 'client', 'components'),
      ],
      use: [{
        loader: 'ng-annotate-loader'
      }, {
        loader: 'babel-loader',
        options: {
          presets: ['env', 'react'],
          cacheDirectory: path.resolve(__dirname, '.tmp', '.babel-cache'),
          plugins: [
            require('babel-plugin-transform-object-rest-spread')
          ]
        }
      }]
    }, {
      test: /\.html$/,
      include: [
        path.resolve(__dirname, 'client', 'views'),
        path.resolve(__dirname, 'client', 'components'),
      ],
      use: 'raw-loader'
    }, {
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader',
          options: {
            minimize: false,
            modules: true,
            localIdentName: '[name]__[local]___[hash:base64:5]'
          }
        }]
      })
    }, {
      test: /\.svg$/,
      loader: 'raw-loader'
    }]
  },
  plugins: [
    new webpack.DefinePlugin({
      process: {
        env: {
          NODE_ENV: JSON.stringify(environment),
          FACEBOOK_ID: JSON.stringify(process.env.FACEBOOK_ID),
          ANALYTICS_ENABLED: JSON.stringify(isProduction),
          BOT_WIDGET_URL: JSON.stringify(`${botWidgetUrl}/embed.js`),
          WEB_MESSENGER_SHORT_URL: JSON.stringify(`${webMessengerShortUrl}/m`),
        }
      }
    }),
    new webpack.DllReferencePlugin({
      context: __dirname,
      manifest: path.resolve(__dirname, '.tmp', 'scripts', 'vendor-manifest.json'),
      name: 'FXO_VENDOR'
    }),
    new ExtractTextPlugin('../stage/components.css'),
  ]
});
