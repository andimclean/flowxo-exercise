'use strict';

var webpackConfig = require('./webpack.app.js');
// webpackConfig.entry = {};

module.exports = function(config) {
  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: '',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
      '.tmp/scripts/vendor.js',
      '.tmp/scripts/app.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'test/helpers.js',
      'test/client/helpers.js',
      'test/client/mocks.js',
      'test/client/mocks/**/*.js',
      'test/client/spec/**/*.js',
      'client/components/**/*.spec.js'
    ],

    // generate js files from html templates
    preprocessors: {
      'test/client/spec/**/*.js': ['babel', 'webpack'],
      'client/components/**/*.spec.js': ['babel', 'webpack']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      noInfo: true
    },

    // web server port
    port: 8080,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: true,

    browserConsoleLogOptions: {
      level: 'log',
      format: '%b %T: %m',
      terminal: true
    },

    client: {
      captureConsole: true
    }
  });
};
