// Custom Gruntfile based on generator-angular-fullstack 2.0.13
'use strict';

module.exports = function (grunt) {
  // Check we are using the correct version of node
  var path = require('path'),
      semver = require('semver'),
      pjson = require('./package.json');

  require('./server/globals');

  if (!semver.satisfies(process.versions.node, pjson.engines.node)) {
    grunt.fatal('Incorrect Node version: got ' + process.versions.node + ', wanted ' + pjson.engines.node + '\n');
  }

  // Fix grunt options
  // Can remove if using grunt 0.5
  // https://github.com/gruntjs/grunt/issues/908
  require('nopt-grunt-fix')(grunt);

  // Load grunt tasks automatically, when needed
  require('jit-grunt')(grunt, {
    express: 'grunt-express-server',
    useminPrepare: 'grunt-usemin',
    protractor: 'grunt-protractor-runner',
    buildcontrol: 'grunt-build-control',
    responsive_images: 'grunt-responsive-images',
    jasmine_node: 'grunt-jasmine-node-coverage',
    cacheBust: 'grunt-cache-bust'
  });

  // Time how long tasks take. Can help when optimizing build times
  if(!grunt.option('no-time')) {
    require('time-grunt')(grunt);
  }

  // Log out fatal exceptions.
  process.on('uncaughtException', function(err) {
    grunt.fatal(err.stack || err.toString());
  });

  var isCI = grunt.option('ci');

  // Define spec verbosities
  var clientSpecVerbosity, serverSpecVerbosity;

  if(isCI) {
    clientSpecVerbosity = 'minimal';
    serverSpecVerbosity = 1;
  } else {
    clientSpecVerbosity = 'full';
    serverSpecVerbosity =  grunt.option('server-spec-verbosity') ?
      +grunt.option('server-spec-verbosity') :
      3;
  }

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    pkg: grunt.file.readJSON('package.json'),
    express: {
      dev: {
        options: {
          script: 'server/app.js'
        }
      },
      prod: {
        options: {
          script: 'dist/server/app.js'
        }
      }
    },
    watch: {
      options: {
        interval: 200
      },
      serverTest: {
        files: [
          'test/server/**/*.js'
        ],
        tasks: ['env:all', 'env:test', 'newer:jshint:server', 'jasmine_node:specs']
      },
      serverCoverage: {
        files: [
          'test/server/**/*.js'
        ],
        tasks: ['env:all', 'env:test', 'newer:jshint:server', 'jasmine_node:coverage']
      },
      clientTest: {
        files: ['client/components/**/*.spec.js', 'test/client/spec/**/*.js'],
        tasks: ['newer:jshint:client', 'webpack:app', 'karma:specs']
      },
      app: {
        files: [
          'client/scripts/**/*.js',
          'client/components/**/*.{js,jsx,css}',
          'client/views/partials/**/*.html'
        ],
        tasks: ['webpack:app']
      },
      less: {
        files: ['client/styles/**/*.less', '.tmp/stage/**/*.css'],
        tasks: ['less:dev']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        files: [
          '{.tmp,client}/styles/**/*.css',
          '{.tmp,client}/views/**/*.html',
          '{.tmp,client}/scripts/**/*.js',
          'client/images/**/*.{png,jpg,jpeg,gif,webp,svg}'
        ],
        options: {
          livereload: true
        }
      },
      express: {
        files: [
          '.env',
          'server/**/*.{js,json}'
        ],
        tasks: ['express:dev', 'wait'],
        options: {
          livereload: true,
          nospawn: true //Without this option specified express won't be reloaded
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        reporter: require('jshint-stylish')
      },
      client: {
        options: {
          jshintrc: 'client/.jshintrc'
        },
        src: [
          'client/scripts/**/*.js',
        ]
      },
      server: {
        options: {
          jshintrc: 'server/.jshintrc'
        },
        src: [
          'server/**/*.js',
        ]
      },
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: [
          'test/**/*.spec.js',
        ]
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            'dist/*',
          ]
        }]
      },
      distGit: 'dist/.git',
      server: '.tmp'
    },

    // Use nodemon to run server in debug mode with an initial breakpoint
    nodemon: {
      inspect: {
        script: 'server/app.js',
        options: {
          nodeArgs: ['--inspect'],
          ignore: ['node_modules/**'],
          callback: function (nodemon) {
            nodemon.on('log', function (event) {
              console.log(event.colour);
            });
          }
        }
      }
    },

    cacheBust: {
      dist: {
        options: {
          assets: [
            'scripts/*.js',
            '!scripts/embed*.js',
            'styles/*.css'
          ],
          baseDir: 'dist/public',
          deleteOriginals: true,
          length: 10,
        },
        src: [
          'dist/views/**/*.html'
        ]
      }
    },

    // The following *-min tasks produce minified files
    imagemin: {
      images: {
        files: [{
          expand: true,
          cwd: 'client/images',
          src: '**/*.{png,jpg,jpeg,gif}',
          dest: 'client/images'
        }]
      }
    },

    // Optimise SVGs
    svgmin: {
      options: {
        plugins: [{
          collapseGroups: false
        }, {
          removeUnknownsAndDefaults: false
        }]
      },
      images: {
        files: [{
          expand: true,
          cwd: 'client/images',
          src: '{,*/}*.svg',
          dest: 'client/images'
        }]
      },
    },

    // Copies remaining files to places other tasks can use
    copy: {
      icons: {
        src: 'client/icons/generated/svg-icons-data.css',
        dest: 'client/styles/app/modules/icons/svg-icons-data.less',
      },
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'client',
          dest: 'dist/public',
          src: [
            '*.{ico,png,txt}',
            'images/**/*.{png,jpg,jpeg,gif,webp,svg}',
            'fonts/*.{eot,svg,ttf,woff}',
            'scripts/embed*.js'
          ]
        }, {
          expand: true,
          dot: true,
          cwd: 'client/views',
          dest: 'dist/views',
          src: ['**/*.html']
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: 'dist/public/images',
          src: ['generated/*']
        }, {
          expand: true,
          dot: true,
          cwd: '.tmp/scripts',
          dest: 'dist/public/scripts',
          src: '**/*.js'
        }, {
          expand: true,
          dest: 'dist',
          src: [
            'package.json',
            'Procfile',
            'server/**/*',
            'scripts/deploy/**/*',
            'scripts/startup.sh',
          ]
        }]
      },
    },

    buildcontrol: {
      options: {
        dir: 'dist',
        commit: true,
        push: true,
        connectCommits: false,
        fetchProgress: !isCI,
        message: process.env.BUILD_COMMIT_MSG || 'Built %sourceName% from commit %sourceCommit% on branch %sourceBranch%'
      },
      staging: {
        options: {
          remote: 'git@heroku.com:flowxo-staging-59237634.git',
          branch: 'master'
        }
      },
      production: {
        options: {
          remote: 'git@heroku.com:flowxo-81502537.git',
          branch: 'master'
        }
      },
      sandbox: {
        options: {
          remote: 'git@heroku.com:flowxo-sandbox-523511.git',
          branch: 'master',
        }
      }
    },

    // Test settings
    karma: {
      options: {
        configFile: 'karma.conf.js',
        mochaReporter: {
          ignoreSkipped: true,
          output: clientSpecVerbosity
        },
        colors: !isCI
      },
      specs: {
        reporters: ['mocha'],
      },
      coverage: {
        reporters: ['mocha', 'coverage'],
        coverageReporter: {
          dir: 'coverage/client',
          reporters: isCI ? [{type: 'text-summary' }] : [{ type: 'lcov' }, {type: 'text' }]
        }
      }
    },

    protractor: {
      options: {
        configFile: 'protractor.conf.js'
      },
      chrome: {
        options: {
          args: {
            browser: 'chrome'
          }
        }
      }
    },

    jasmine_node: {
      options: {
        forceExit: true,
        jasmine: {
          spec_dir: 'test',
          helpers: [
            'helpers.js',
            'server/mocks.js',
            'server/bootstrap.js'
          ],
          spec_files: [
            'server/unit/**/*.js'
          ],
          reporter: {
            colors: {
              enabled: !isCI
            },
            summary: {
              displayStacktrace: true
            },
            spec: {
              displaySuccessful: !isCI
            }
          }
        }
      },
      specs: {
        options: {
          coverage: false
        }
      },
      coverage: {
        options: {
          coverage: {
            report: isCI ? ['text-summary'] : ['lcov', 'text'],
            reportDir: 'coverage/server',
            includeAllSources: true,
            excludes: [
              'server/bootstrap/**',
              'server/config/**',
              'server/controllers/index.js',
              'server/lifecycle/index.js',
              'server/lifecycle/internal/index.js',
              'server/lifecycle/external/index.js',
              'server/queue/**',
              'server/routes/**',
              'server/services/**',
              'server/app.js',
              'server/checkSecrets.js',
              'server/error-reporter.js',
              'server/globals.js',
              'server/gracefuls.js',
              'server/heartbeat.js',
              'server/instrumentor.js',
              'server/logger.js',
              'server/newrelic.js',
              'server/patches.js',
              'server/serviceInstaller.js',
            ],
            watermarks: {
              statements: [80, 90],
              lines: [80, 90],
              functions: [80, 90],
              branches: [80, 90],
            },
            thresholds: {
              statements: 0,
              lines: 0,
              functions: 0,
              branches: 0
            }
          }
        },
        src: [
          'server/**/*.js'
        ]
      }
    },

    open: {
      'coverage-server': {
        path: __dirname + '/coverage/server/lcov-report/index.html'
      }
    },

    env: {
      test: {
        PORT: 9002,
        NODE_ENV: 'test',
        NEW_RELIC_HOME: __dirname + '/server'
      },
      prod: {
        NODE_ENV: 'production'
      },
      all: {
        src: '.env'
      }
    },

    // Compiles Less to CSS
    less: {
      options: {
        paths: [
          'client/styles',
          'node_modules',
          '.tmp/stage'
        ],
      },
      dev: {
        files: [{
          expand: true,
          cwd: 'client/styles',
          src: '*.less',
          dest: '.tmp/styles',
          ext: '.css'
        }],
        options: {
          dumpLineNumbers: 'comments'
        },
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'client/styles',
          src: '*.less',
          dest: 'dist/public/styles',
          ext: '.css'
        }],
        options: {
          plugins: [
            new (require('less-plugin-autoprefix'))({
              browsers: ['last 2 versions']
            }),
            new (require('less-plugin-clean-css'))()
          ],
        }
      }
    },

    prompt: {
      confirmDeployLive: {
        options: {
          questions: [{
            config: 'deploy.production.confirm',
            type: 'input',
            message: 'You are about to deploy to the production environment. Type \'flowxo\' to confirm you want to do this!'
          }],
          then: function(results) {
            if(results['deploy.production.confirm'] !== 'flowxo') {
              grunt.fatal('Operation aborted.');
            }
          }
        }
      }
    }
  });

  // Used for delaying livereload until after server has restarted
  grunt.registerTask('wait', function () {
    grunt.log.ok('Waiting for server reload...');

    var done = this.async();

    setTimeout(function () {
      grunt.log.writeln('Done waiting!');
      done();
    }, 1500);
  });

  grunt.registerTask('express-keepalive', 'Keep grunt running', function() {
    this.async();
  });

  grunt.registerTask('serve', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'env:all', 'env:prod', 'express:prod', 'express-keepalive']);
    }

    if (target === 'inspect') {
      return grunt.task.run([
        'clean:server',
        'env:all',
        'webpack:dll',
        'webpack:app',
        'less:dev',
        'nodemon:inspect'
      ]);
    }

    if (target === 'webstorm-livereload') {
      return grunt.task.run([
        'clean:server',
        'env:all',
        'webpack:dll',
        'webpack:app',
        'less:dev',
        'watch:livereload'
      ]);
    }

    grunt.task.run([
      'clean:server',
      'env:all',
      'webpack:dll',
      'webpack:app',
      'less:dev',
      'express:dev',
      'watch'
    ]);
  });

  grunt.registerTask('server', function () {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve']);
  });

  grunt.registerTask('test', function(target) {
    if (target === 'server') {
      return grunt.task.run([
        'env:all',
        'env:test',
        'jasmine_node:coverage'
      ]);
    }

    else if (target === 'serverWatch') {
      return grunt.task.run('watch:serverTest');
    }

    else if (target === 'client') {
      return grunt.task.run([
        'clean:server',
        'env:all',
        'webpack:dll',
        'webpack:app',
        'karma:specs'
      ]);
    }

    else if (target === 'clientWatch') {
      return grunt.task.run([
        'clean:server',
        'env:all',
        'webpack:dll',
        'webpack:app',
        'watch:clientTest'
      ]);
    }

    else if (target === 'e2e') {
      return grunt.task.run([
        'clean:server',
        'env:all',
        'env:test',
        'less:dev',
        'express:dev',
        'protractor'
      ]);
    }

    else grunt.task.run([
      'test:server',
      'test:client'
    ]);
  });

  grunt.registerTask('spawnTest', function(target) {
    var args = ['test'];
    if(target) {
      args[0] += ':' + target;
    }

    if(isCI) {
      args.push('--ci');
    }

    if(grunt.option('no-color')) {
      args.push('--no-color');
    }

    args.push('--no-time');

    var done = this.async();

    grunt.log.subhead('*** Spawning tests in new process ***');
    grunt.log.writeln('');

    grunt.util.spawn({
      grunt: true,
      args: args,
      opts: {
        stdio: 'inherit'
      }
    }, function(err) {
      grunt.log.subhead('*** Tests finished ***');
      grunt.log.writeln('');

      if(err) {
        return done(new Error('Tests failed: see messages above for details.'))
      }
      done();
    });
  });

  grunt.registerTask('webpack', function(target) {
    // We are compiling using grunt spawning because
    // we are unable to use the vendor DLL plugin
    // in the way that we want to.
    var args = ['--hide-modules', `--config=webpack.${target}.js`];
    if(!isCI) {
      args.push('--progress');
    }
    var done = this.async();
    grunt.util.spawn({
      cmd: path.join('node_modules', '.bin', 'webpack'),
      args,
      opts: {
        stdio: 'inherit'
      }
    }, done);
  });

  grunt.registerTask('preflight', [ 'jshint', 'test' ]);

  function setIconSizes(iconFiles) {
    if(iconFiles.length === 0) {
      // Nothing to do.
      grunt.log.writeln('No icons to process.');
      return;
    }

    // Build up our service icons sizes.
    var iconsConfig = require('./server/config/icons/definition');
    var iconsCombinations = [{
      rename: false,
      width: iconsConfig.default_width
    }];

    iconsConfig.available_widths.forEach(function(w) {
      iconsConfig.available_pixel_ratios.forEach(function(pr) {
        if(pr === 1) {
          iconsCombinations.push({
            name: w.toString(),
            width: w
          });
        } else {
          iconsCombinations.push({
            name: `${w}-${pr}x`,
            width: w * pr,
            quality: 60
          });
        }
      });
    });

    // Set our options, then run the task.
    grunt.config.set('responsive_images.icons.options.sizes', iconsCombinations);
    grunt.config.set('responsive_images.icons.files', iconFiles);

    grunt.log.writeln('');
    grunt.log.writeln('Generating responsive icons, please wait. This might take a while.');

    grunt.task.run(['responsive_images', 'newer:imagemin']);
  }

  grunt.registerTask('generateServiceIcons', function() {
    var getServiceIconsFileMapping = require('./scripts/service-icons/find-service-icons');
    var serviceIconsFiles = getServiceIconsFileMapping('client/images/service-icons');

    setIconSizes(serviceIconsFiles);
  });

  grunt.registerTask('generateTemplateIcons', function() {
    var templates = require('./server/config/templates/available');
    var iconsFiles = templates.map(function(template) {
      return {
        src: `icons/templates/${template.key}.png`,
        dest: `client/images/template-icons/${template.key}/icon.png`
      };
    });

    setIconSizes(iconsFiles);
  });


  grunt.registerTask('build', [
    'env:prod',
    'clean:dist',
    'webpack:dll',
    'webpack:app',
    'less:dist',
    'copy:dist',
    'cacheBust:dist'
  ]);

  grunt.registerTask('sandbox-deploy',[
    'build',
    'buildcontrol:sandbox']
  );

  grunt.registerTask('deploy', function(target) {
    if(!process.env.FACEBOOK_ID) {
      throw new Error('FACEBOOK_ID must be set.');
    }

    var tasks = [];

    var isStaging = /^stage|staging$/.test(target);

    if(!isStaging && !isCI) {
      tasks.push('prompt:confirmDeployLive');
    }

    if(grunt.option('test')) {
      tasks.push('jshint');
      tasks.push('spawnTest');
    } else {
      grunt.log.writeln('Skipping tests.');
    }

    tasks.push('build');

    if(isStaging) {
      tasks.push('buildcontrol:staging');

    } else if(target === 'production') {
      tasks.push('buildcontrol:production');

    } else {
      tasks.push('buildcontrol:staging');
      // Remove .git folder for new remote
      tasks.push('clean:distGit');
      tasks.push('buildcontrol:production');
    }

    grunt.task.run(tasks);
  });

  grunt.registerTask('view-coverage', ['open']);

  grunt.registerTask('default', [
    'preflight',
    'build'
  ]);
};
