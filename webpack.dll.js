'use strict';

const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const path = require('path');
const baseConfig = require('./webpack.config.js');

module.exports = webpackMerge(baseConfig, {
  entry: {
    vendor: [path.resolve(__dirname, 'client', 'scripts', 'app', 'vendor.js')]
  },
  output: {
    library: 'FXO_VENDOR'
  },
  plugins: [
    new webpack.DllPlugin({
      path: path.resolve(__dirname, '.tmp', 'scripts', 'vendor-manifest.json'),
      name: 'FXO_VENDOR',
      context: '.'
    })
  ]
});