# README #

## A simple app for taking notes. ##

### Specification ###

Frontend : Angular

Backend : Node.js, Mongodb

Each Note should store:

* Text
* Created Date
* Edited Date


You should be able to:

* Add
* Edit
* Delete

Main screen should list notes with preview text and be sorted by created or edited date.
Second screen for editing note.

Add usage analytics. How and what to track and display is up to you.

### Structure ###

Boilerplate provides

* express server
* basic mongo config
* angular 1 for client
* grunt and webpack building auto loading
* client and server side tests


### Requirements ###

* mongodb running on the default local port (27017)
* grunt-cli installed globally via npm

### Usage ###

Usage of the boilerplate


`npm install` in root folder.

`grunt serve` will start the app

`grunt test` will run tests


## Windows Users ##

* Some windows users have experienced issues installing packages that use the [ node-gyp ](https://github.com/nodejs/node-gyp), theres information on their github page on what tools to install for this to work.
* Some windows users have experienced issues running the grunt commands becuase of an incompatibility with 'grunt spawn'. Please use the replacement files that are prefixed with '-windows' in the root of this repository if you are experiencing this issue. 
