;(function() {
  'use strict';

  var element = document.getElementById('resizable');
  var parent = element.parentNode;
  var words = element.textContent.split(' ');
  var maxFontSize = 20;
  var minFontSize = 16;
  var resizeThrottleInterval = 200;

  // Set the handler
  window.onresize = throttle(onResize, resizeThrottleInterval);

  // Init
  onResize();

  function onResize() {
    var fontSize = maxFontSize;
    var parentHeight = parent.getBoundingClientRect().height;

    element.style.fontSize = fontSize + 'px';
    element.textContent = words.join(' ');

    while(element.getBoundingClientRect().height > parentHeight && fontSize >= minFontSize) {
      fontSize -= 1;
      element.style.fontSize = fontSize + 'px';
    }

    var currentWord = words.length;

    while(element.getBoundingClientRect().height > parentHeight && currentWord > 0) {
      currentWord -= 1;
      element.textContent = words.slice(0, currentWord).join(' ') + '...';
    }
  }

  function throttle(fn, time) {
    var waiting = false;
    var timeout;

    return function handler() {
      if(timeout) {
        waiting = true;
        return;
      }

      fn();

      timeout = setTimeout(() => {
        timeout = 0;

        if(waiting) {
          waiting = false;
          handler();
        }
      }, time);
    };
  }

})();