'use strict';

function requireAll(context) {
  context.keys().forEach(context);
}

// Load all of our dependencies
require('./vendor.js');

// Load some prerequisites
requireAll(require.context('./utils', true));
requireAll(require.context('./vendor', true));

// Load our (new) components and existing angular app
require('./modules.js');
requireAll(require.context('./constants', true));
requireAll(require.context('./controllers', true));
requireAll(require.context('./directives', true));
requireAll(require.context('./filters', true));
requireAll(require.context('./resources', true));
requireAll(require.context('./services', true));
