// App dependencies are split into a different bundle.

'use strict';

// Polyfills
require('es6-promise').polyfill();
require('isomorphic-fetch');
require('regenerator-runtime/runtime');

// jQuery plugins and angular expect jQuery to be global.
// We also use `$` in a few places in the app.
window.$ = window.jQuery = require('jquery');
require('jquery-ui');

// angular-daterange-picker uses a global moment object
window.moment = require('moment');

// We use the following as globals in the app.
window.angular = require('angular');
window._ = require('lodash');

require('imager.js');
require('js-base64');
require('md5');
require('querystring');

require('@claviska/jquery-minicolors');
require('bootstrap-daterangepicker');

require('angular-route');
require('angular-easyfb');
require('angular-elastic');
require('angular-gettext');
require('angular-loading-bar');
require('angular-ui-bootstrap');
require('ng-infinite-scroll');
require('ui-select');
require('angulartics');
require('angulartics-google-tag-manager');

var hljs = require('hljs');
hljs.registerLanguage('javascript', require('highlight.js/lib/languages/javascript'));
hljs.registerLanguage('json', require('highlight.js/lib/languages/json'));
hljs.registerLanguage('xml', require('highlight.js/lib/languages/xml'));
