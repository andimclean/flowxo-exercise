'use strict';

angular
  .module('flowxo-core')
  .constant('timezones', [{
    region: 'UTC',
    label: 'UTC',
    value: 'UTC'
  },  {
    value: 'Europe/Amsterdam',
    region: 'Europe',
    label: 'Amsterdam'
  }, {
    value: 'Europe/Andorra',
    region: 'Europe',
    label: 'Andorra'
  }, {
    value: 'Europe/Athens',
    region: 'Europe',
    label: 'Athens'
  }, {
    value: 'Europe/Belgrade',
    region: 'Europe',
    label: 'Belgrade'
  }, {
    value: 'Europe/Berlin',
    region: 'Europe',
    label: 'Berlin'
  }, {
    value: 'Europe/Bratislava',
    region: 'Europe',
    label: 'Bratislava'
  }, {
    value: 'Europe/Brussels',
    region: 'Europe',
    label: 'Brussels'
  }, {
    value: 'Europe/Bucharest',
    region: 'Europe',
    label: 'Bucharest'
  }, {
    value: 'Europe/Budapest',
    region: 'Europe',
    label: 'Budapest'
  }, {
    value: 'Europe/Chisinau',
    region: 'Europe',
    label: 'Chisinau'
  }, {
    value: 'Europe/Copenhagen',
    region: 'Europe',
    label: 'Copenhagen'
  }, {
    value: 'Europe/Dublin',
    region: 'Europe',
    label: 'Dublin'
  }, {
    value: 'Europe/Gibraltar',
    region: 'Europe',
    label: 'Gibraltar'
  }, {
    value: 'Europe/Guernsey',
    region: 'Europe',
    label: 'Guernsey'
  }, {
    value: 'Europe/Helsinki',
    region: 'Europe',
    label: 'Helsinki'
  }, {
    value: 'Europe/Isle_of_Man',
    region: 'Europe',
    label: 'Isle of Man'
  }, {
    value: 'Europe/Istanbul',
    region: 'Europe',
    label: 'Istanbul'
  }, {
    value: 'Europe/Jersey',
    region: 'Europe',
    label: 'Jersey'
  }, {
    value: 'Europe/Kaliningrad',
    region: 'Europe',
    label: 'Kaliningrad'
  }, {
    value: 'Europe/Kiev',
    region: 'Europe',
    label: 'Kiev'
  }, {
    value: 'Europe/Lisbon',
    region: 'Europe',
    label: 'Lisbon'
  }, {
    value: 'Europe/Ljubljana',
    region: 'Europe',
    label: 'Ljubljana'
  }, {
    value: 'Europe/London',
    region: 'Europe',
    label: 'London'
  }, {
    value: 'Europe/Luxembourg',
    region: 'Europe',
    label: 'Luxembourg'
  }, {
    value: 'Europe/Madrid',
    region: 'Europe',
    label: 'Madrid'
  }, {
    value: 'Europe/Malta',
    region: 'Europe',
    label: 'Malta'
  }, {
    value: 'Europe/Mariehamn',
    region: 'Europe',
    label: 'Mariehamn'
  }, {
    value: 'Europe/Minsk',
    region: 'Europe',
    label: 'Minsk'
  }, {
    value: 'Europe/Monaco',
    region: 'Europe',
    label: 'Monaco'
  }, {
    value: 'Europe/Moscow',
    region: 'Europe',
    label: 'Moscow'
  }, {
    value: 'Europe/Oslo',
    region: 'Europe',
    label: 'Oslo'
  }, {
    value: 'Europe/Paris',
    region: 'Europe',
    label: 'Paris'
  }, {
    value: 'Europe/Podgorica',
    region: 'Europe',
    label: 'Podgorica'
  }, {
    value: 'Europe/Prague',
    region: 'Europe',
    label: 'Prague'
  }, {
    value: 'Europe/Riga',
    region: 'Europe',
    label: 'Riga'
  }, {
    value: 'Europe/Rome',
    region: 'Europe',
    label: 'Rome'
  }, {
    value: 'Europe/Samara',
    region: 'Europe',
    label: 'Samara'
  }, {
    value: 'Europe/San_Marino',
    region: 'Europe',
    label: 'San Marino'
  }, {
    value: 'Europe/Sarajevo',
    region: 'Europe',
    label: 'Sarajevo'
  }, {
    value: 'Europe/Simferopol',
    region: 'Europe',
    label: 'Simferopol'
  }, {
    value: 'Europe/Skopje',
    region: 'Europe',
    label: 'Skopje'
  }, {
    value: 'Europe/Sofia',
    region: 'Europe',
    label: 'Sofia'
  }, {
    value: 'Europe/Stockholm',
    region: 'Europe',
    label: 'Stockholm'
  }, {
    value: 'Europe/Tallinn',
    region: 'Europe',
    label: 'Tallinn'
  }, {
    value: 'Europe/Tirane',
    region: 'Europe',
    label: 'Tirane'
  }, {
    value: 'Europe/Uzhgorod',
    region: 'Europe',
    label: 'Uzhgorod'
  }, {
    value: 'Europe/Vaduz',
    region: 'Europe',
    label: 'Vaduz'
  }, {
    value: 'Europe/Vatican',
    region: 'Europe',
    label: 'Vatican'
  }, {
    value: 'Europe/Vienna',
    region: 'Europe',
    label: 'Vienna'
  }, {
    value: 'Europe/Vilnius',
    region: 'Europe',
    label: 'Vilnius'
  }, {
    value: 'Europe/Volgograd',
    region: 'Europe',
    label: 'Volgograd'
  }, {
    value: 'Europe/Warsaw',
    region: 'Europe',
    label: 'Warsaw'
  }, {
    value: 'Europe/Zagreb',
    region: 'Europe',
    label: 'Zagreb'
  }, {
    value: 'Europe/Zaporozhye',
    region: 'Europe',
    label: 'Zaporozhye'
  }, {
    value: 'Europe/Zurich',
    region: 'Europe',
    label: 'Zurich'
  }, {
    value: 'America/Adak',
    region: 'America',
    label: 'Adak'
  }, {
    value: 'America/Anchorage',
    region: 'America',
    label: 'Anchorage'
  }, {
    value: 'America/Anguilla',
    region: 'America',
    label: 'Anguilla'
  }, {
    value: 'America/Antigua',
    region: 'America',
    label: 'Antigua'
  }, {
    value: 'America/Araguaina',
    region: 'America',
    label: 'Araguaina'
  }, {
    value: 'America/Argentina/Buenos_Aires',
    region: 'America',
    label: 'Argentina - Buenos Aires'
  }, {
    value: 'America/Argentina/Catamarca',
    region: 'America',
    label: 'Argentina - Catamarca'
  }, {
    value: 'America/Argentina/Cordoba',
    region: 'America',
    label: 'Argentina - Cordoba'
  }, {
    value: 'America/Argentina/Jujuy',
    region: 'America',
    label: 'Argentina - Jujuy'
  }, {
    value: 'America/Argentina/La_Rioja',
    region: 'America',
    label: 'Argentina - La Rioja'
  }, {
    value: 'America/Argentina/Mendoza',
    region: 'America',
    label: 'Argentina - Mendoza'
  }, {
    value: 'America/Argentina/Rio_Gallegos',
    region: 'America',
    label: 'Argentina - Rio Gallegos'
  }, {
    value: 'America/Argentina/Salta',
    region: 'America',
    label: 'Argentina - Salta'
  }, {
    value: 'America/Argentina/San_Juan',
    region: 'America',
    label: 'Argentina - San Juan'
  }, {
    value: 'America/Argentina/San_Luis',
    region: 'America',
    label: 'Argentina - San Luis'
  }, {
    value: 'America/Argentina/Tucuman',
    region: 'America',
    label: 'Argentina - Tucuman'
  }, {
    value: 'America/Argentina/Ushuaia',
    region: 'America',
    label: 'Argentina - Ushuaia'
  }, {
    value: 'America/Aruba',
    region: 'America',
    label: 'Aruba'
  }, {
    value: 'America/Asuncion',
    region: 'America',
    label: 'Asuncion'
  }, {
    value: 'America/Atikokan',
    region: 'America',
    label: 'Atikokan'
  }, {
    value: 'America/Bahia',
    region: 'America',
    label: 'Bahia'
  }, {
    value: 'America/Bahia_Banderas',
    region: 'America',
    label: 'Bahia Banderas'
  }, {
    value: 'America/Barbados',
    region: 'America',
    label: 'Barbados'
  }, {
    value: 'America/Belem',
    region: 'America',
    label: 'Belem'
  }, {
    value: 'America/Belize',
    region: 'America',
    label: 'Belize'
  }, {
    value: 'America/Blanc-Sablon',
    region: 'America',
    label: 'Blanc-Sablon'
  }, {
    value: 'America/Boa_Vista',
    region: 'America',
    label: 'Boa Vista'
  }, {
    value: 'America/Bogota',
    region: 'America',
    label: 'Bogota'
  }, {
    value: 'America/Boise',
    region: 'America',
    label: 'Boise'
  }, {
    value: 'America/Cambridge_Bay',
    region: 'America',
    label: 'Cambridge Bay'
  }, {
    value: 'America/Campo_Grande',
    region: 'America',
    label: 'Campo Grande'
  }, {
    value: 'America/Cancun',
    region: 'America',
    label: 'Cancun'
  }, {
    value: 'America/Caracas',
    region: 'America',
    label: 'Caracas'
  }, {
    value: 'America/Cayenne',
    region: 'America',
    label: 'Cayenne'
  }, {
    value: 'America/Cayman',
    region: 'America',
    label: 'Cayman'
  }, {
    value: 'America/Chicago',
    region: 'America',
    label: 'Chicago'
  }, {
    value: 'America/Chihuahua',
    region: 'America',
    label: 'Chihuahua'
  }, {
    value: 'America/Costa_Rica',
    region: 'America',
    label: 'Costa Rica'
  }, {
    value: 'America/Creston',
    region: 'America',
    label: 'Creston'
  }, {
    value: 'America/Cuiaba',
    region: 'America',
    label: 'Cuiaba'
  }, {
    value: 'America/Curacao',
    region: 'America',
    label: 'Curacao'
  }, {
    value: 'America/Dawson',
    region: 'America',
    label: 'Dawson'
  }, {
    value: 'America/Dawson_Creek',
    region: 'America',
    label: 'Dawson Creek'
  }, {
    value: 'America/Denver',
    region: 'America',
    label: 'Denver'
  }, {
    value: 'America/Detroit',
    region: 'America',
    label: 'Detroit'
  }, {
    value: 'America/Dominica',
    region: 'America',
    label: 'Dominica'
  }, {
    value: 'America/Edmonton',
    region: 'America',
    label: 'Edmonton'
  }, {
    value: 'America/El_Salvador',
    region: 'America',
    label: 'El Salvador'
  }, {
    value: 'America/Fortaleza',
    region: 'America',
    label: 'Fortaleza'
  }, {
    value: 'America/Glace_Bay',
    region: 'America',
    label: 'Glace Bay'
  }, {
    value: 'America/Godthab',
    region: 'America',
    label: 'Godthab'
  }, {
    value: 'America/Goose_Bay',
    region: 'America',
    label: 'Goose Bay'
  }, {
    value: 'America/Grand_Turk',
    region: 'America',
    label: 'Grand Turk'
  }, {
    value: 'America/Grenada',
    region: 'America',
    label: 'Grenada'
  }, {
    value: 'America/Guadeloupe',
    region: 'America',
    label: 'Guadeloupe'
  }, {
    value: 'America/Guatemala',
    region: 'America',
    label: 'Guatemala'
  }, {
    value: 'America/Guayaquil',
    region: 'America',
    label: 'Guayaquil'
  }, {
    value: 'America/Guyana',
    region: 'America',
    label: 'Guyana'
  }, {
    value: 'America/Halifax',
    region: 'America',
    label: 'Halifax'
  }, {
    value: 'America/Havana',
    region: 'America',
    label: 'Havana'
  }, {
    value: 'America/Hermosillo',
    region: 'America',
    label: 'Hermosillo'
  }, {
    value: 'America/Indiana/Indianapolis',
    region: 'America',
    label: 'Indiana - Indianapolis'
  }, {
    value: 'America/Indiana/Knox',
    region: 'America',
    label: 'Indiana - Knox'
  }, {
    value: 'America/Indiana/Marengo',
    region: 'America',
    label: 'Indiana - Marengo'
  }, {
    value: 'America/Indiana/Petersburg',
    region: 'America',
    label: 'Indiana - Petersburg'
  }, {
    value: 'America/Indiana/Tell_City',
    region: 'America',
    label: 'Indiana - Tell City'
  }, {
    value: 'America/Indiana/Vevay',
    region: 'America',
    label: 'Indiana - Vevay'
  }, {
    value: 'America/Indiana/Vincennes',
    region: 'America',
    label: 'Indiana - Vincennes'
  }, {
    value: 'America/Indiana/Winamac',
    region: 'America',
    label: 'Indiana - Winamac'
  }, {
    value: 'America/Inuvik',
    region: 'America',
    label: 'Inuvik'
  }, {
    value: 'America/Iqaluit',
    region: 'America',
    label: 'Iqaluit'
  }, {
    value: 'America/Jamaica',
    region: 'America',
    label: 'Jamaica'
  }, {
    value: 'America/Juneau',
    region: 'America',
    label: 'Juneau'
  }, {
    value: 'America/Kentucky/Louisville',
    region: 'America',
    label: 'Kentucky - Louisville'
  }, {
    value: 'America/Kentucky/Monticello',
    region: 'America',
    label: 'Kentucky - Monticello'
  }, {
    value: 'America/Kralendijk',
    region: 'America',
    label: 'Kralendijk'
  }, {
    value: 'America/La_Paz',
    region: 'America',
    label: 'La Paz'
  }, {
    value: 'America/Lima',
    region: 'America',
    label: 'Lima'
  }, {
    value: 'America/Los_Angeles',
    region: 'America',
    label: 'Los Angeles'
  }, {
    value: 'America/Lower_Princes',
    region: 'America',
    label: 'Lower Princes'
  }, {
    value: 'America/Maceio',
    region: 'America',
    label: 'Maceio'
  }, {
    value: 'America/Managua',
    region: 'America',
    label: 'Managua'
  }, {
    value: 'America/Manaus',
    region: 'America',
    label: 'Manaus'
  }, {
    value: 'America/Marigot',
    region: 'America',
    label: 'Marigot'
  }, {
    value: 'America/Martinique',
    region: 'America',
    label: 'Martinique'
  }, {
    value: 'America/Matamoros',
    region: 'America',
    label: 'Matamoros'
  }, {
    value: 'America/Mazatlan',
    region: 'America',
    label: 'Mazatlan'
  }, {
    value: 'America/Menominee',
    region: 'America',
    label: 'Menominee'
  }, {
    value: 'America/Merida',
    region: 'America',
    label: 'Merida'
  }, {
    value: 'America/Metlakatla',
    region: 'America',
    label: 'Metlakatla'
  }, {
    value: 'America/Mexico_City',
    region: 'America',
    label: 'Mexico City'
  }, {
    value: 'America/Miquelon',
    region: 'America',
    label: 'Miquelon'
  }, {
    value: 'America/Moncton',
    region: 'America',
    label: 'Moncton'
  }, {
    value: 'America/Monterrey',
    region: 'America',
    label: 'Monterrey'
  }, {
    value: 'America/Montevideo',
    region: 'America',
    label: 'Montevideo'
  }, {
    value: 'America/Montserrat',
    region: 'America',
    label: 'Montserrat'
  }, {
    value: 'America/Nassau',
    region: 'America',
    label: 'Nassau'
  }, {
    value: 'America/New_York',
    region: 'America',
    label: 'New York'
  }, {
    value: 'America/Nipigon',
    region: 'America',
    label: 'Nipigon'
  }, {
    value: 'America/Nome',
    region: 'America',
    label: 'Nome'
  }, {
    value: 'America/Noronha',
    region: 'America',
    label: 'Noronha'
  }, {
    value: 'America/North_Dakota/Beulah',
    region: 'America',
    label: 'North Dakota - Beulah'
  }, {
    value: 'America/North_Dakota/New_Salem',
    region: 'America',
    label: 'North Dakota - New Salem'
  }, {
    value: 'America/Ojinaga',
    region: 'America',
    label: 'Ojinaga'
  }, {
    value: 'America/Panama',
    region: 'America',
    label: 'Panama'
  }, {
    value: 'America/Pangnirtung',
    region: 'America',
    label: 'Pangnirtung'
  }, {
    value: 'America/Paramaribo',
    region: 'America',
    label: 'Paramaribo'
  }, {
    value: 'America/Phoenix',
    region: 'America',
    label: 'Phoenix'
  }, {
    value: 'America/Port_of_Spain',
    region: 'America',
    label: 'Port of Spain'
  }, {
    value: 'America/Port-au-Prince',
    region: 'America',
    label: 'Port-au-Prince'
  }, {
    value: 'America/Porto_Velho',
    region: 'America',
    label: 'Porto Velho'
  }, {
    value: 'America/Puerto_Rico',
    region: 'America',
    label: 'Puerto Rico'
  }, {
    value: 'America/Rankin_Inlet',
    region: 'America',
    label: 'Rankin Inlet'
  }, {
    value: 'America/Recife',
    region: 'America',
    label: 'Recife'
  }, {
    value: 'America/Regina',
    region: 'America',
    label: 'Regina'
  }, {
    value: 'America/Rio_Branco',
    region: 'America',
    label: 'Rio Branco'
  }, {
    value: 'America/Santa_Isabel',
    region: 'America',
    label: 'Santa Isabel'
  }, {
    value: 'America/Santarem',
    region: 'America',
    label: 'Santarem'
  }, {
    value: 'America/Santiago',
    region: 'America',
    label: 'Santiago'
  }, {
    value: 'America/Santo_Domingo',
    region: 'America',
    label: 'Santo Domingo'
  }, {
    value: 'America/Sao_Paulo',
    region: 'America',
    label: 'Sao Paulo'
  }, {
    value: 'America/Sitka',
    region: 'America',
    label: 'Sitka'
  }, {
    value: 'America/St_Barthelemy',
    region: 'America',
    label: 'St Barthelemy'
  }, {
    value: 'America/St_Johns',
    region: 'America',
    label: 'St Johns'
  }, {
    value: 'America/St_Kitts',
    region: 'America',
    label: 'St Kitts'
  }, {
    value: 'America/St_Lucia',
    region: 'America',
    label: 'St Lucia'
  }, {
    value: 'America/St_Thomas',
    region: 'America',
    label: 'St Thomas'
  }, {
    value: 'America/St_Vincent',
    region: 'America',
    label: 'St Vincent'
  }, {
    value: 'America/Swift_Current',
    region: 'America',
    label: 'Swift Current'
  }, {
    value: 'America/Tegucigalpa',
    region: 'America',
    label: 'Tegucigalpa'
  }, {
    value: 'America/Thule',
    region: 'America',
    label: 'Thule'
  }, {
    value: 'America/Thunder_Bay',
    region: 'America',
    label: 'Thunder Bay'
  }, {
    value: 'America/Tijuana',
    region: 'America',
    label: 'Tijuana'
  }, {
    value: 'America/Toronto',
    region: 'America',
    label: 'Toronto'
  }, {
    value: 'America/Tortola',
    region: 'America',
    label: 'Tortola'
  }, {
    value: 'America/Vancouver',
    region: 'America',
    label: 'Vancouver'
  }, {
    value: 'America/Whitehorse',
    region: 'America',
    label: 'Whitehorse'
  }, {
    value: 'America/Winnipeg',
    region: 'America',
    label: 'Winnipeg'
  }, {
    value: 'America/Yellowknife',
    region: 'America',
    label: 'Yellowknife'
  }, {
    value: 'Africa/Abidjan',
    region: 'Africa',
    label: 'Abidjan'
  }, {
    value: 'Africa/Accra',
    region: 'Africa',
    label: 'Accra'
  }, {
    value: 'Africa/Addis_Ababa',
    region: 'Africa',
    label: 'Addis Ababa'
  }, {
    value: 'Africa/Algiers',
    region: 'Africa',
    label: 'Algiers'
  }, {
    value: 'Africa/Asmara',
    region: 'Africa',
    label: 'Asmara'
  }, {
    value: 'Africa/Bamako',
    region: 'Africa',
    label: 'Bamako'
  }, {
    value: 'Africa/Bangui',
    region: 'Africa',
    label: 'Bangui'
  }, {
    value: 'Africa/Banjul',
    region: 'Africa',
    label: 'Banjul'
  }, {
    value: 'Africa/Bissau',
    region: 'Africa',
    label: 'Bissau'
  }, {
    value: 'Africa/Blantyre',
    region: 'Africa',
    label: 'Blantyre'
  }, {
    value: 'Africa/Brazzaville',
    region: 'Africa',
    label: 'Brazzaville'
  }, {
    value: 'Africa/Bujumbura',
    region: 'Africa',
    label: 'Bujumbura'
  }, {
    value: 'Africa/Cairo',
    region: 'Africa',
    label: 'Cairo'
  }, {
    value: 'Africa/Casablanca',
    region: 'Africa',
    label: 'Casablanca'
  }, {
    value: 'Africa/Ceuta',
    region: 'Africa',
    label: 'Ceuta'
  }, {
    value: 'Africa/Conakry',
    region: 'Africa',
    label: 'Conakry'
  }, {
    value: 'Africa/Dakar',
    region: 'Africa',
    label: 'Dakar'
  }, {
    value: 'Africa/Dar_es_Salaam',
    region: 'Africa',
    label: 'Dar es Salaam'
  }, {
    value: 'Africa/Djibouti',
    region: 'Africa',
    label: 'Djibouti'
  }, {
    value: 'Africa/Douala',
    region: 'Africa',
    label: 'Douala'
  }, {
    value: 'Africa/El_Aaiun',
    region: 'Africa',
    label: 'El Aaiun'
  }, {
    value: 'Africa/Freetown',
    region: 'Africa',
    label: 'Freetown'
  }, {
    value: 'Africa/Gaborone',
    region: 'Africa',
    label: 'Gaborone'
  }, {
    value: 'Africa/Harare',
    region: 'Africa',
    label: 'Harare'
  }, {
    value: 'Africa/Johannesburg',
    region: 'Africa',
    label: 'Johannesburg'
  }, {
    value: 'Africa/Juba',
    region: 'Africa',
    label: 'Juba'
  }, {
    value: 'Africa/Kampala',
    region: 'Africa',
    label: 'Kampala'
  }, {
    value: 'Africa/Khartoum',
    region: 'Africa',
    label: 'Khartoum'
  }, {
    value: 'Africa/Kigali',
    region: 'Africa',
    label: 'Kigali'
  }, {
    value: 'Africa/Kinshasa',
    region: 'Africa',
    label: 'Kinshasa'
  }, {
    value: 'Africa/Lagos',
    region: 'Africa',
    label: 'Lagos'
  }, {
    value: 'Africa/Libreville',
    region: 'Africa',
    label: 'Libreville'
  }, {
    value: 'Africa/Lome',
    region: 'Africa',
    label: 'Lome'
  }, {
    value: 'Africa/Luanda',
    region: 'Africa',
    label: 'Luanda'
  }, {
    value: 'Africa/Lubumbashi',
    region: 'Africa',
    label: 'Lubumbashi'
  }, {
    value: 'Africa/Lusaka',
    region: 'Africa',
    label: 'Lusaka'
  }, {
    value: 'Africa/Malabo',
    region: 'Africa',
    label: 'Malabo'
  }, {
    value: 'Africa/Maputo',
    region: 'Africa',
    label: 'Maputo'
  }, {
    value: 'Africa/Maseru',
    region: 'Africa',
    label: 'Maseru'
  }, {
    value: 'Africa/Mbabane',
    region: 'Africa',
    label: 'Mbabane'
  }, {
    value: 'Africa/Mogadishu',
    region: 'Africa',
    label: 'Mogadishu'
  }, {
    value: 'Africa/Monrovia',
    region: 'Africa',
    label: 'Monrovia'
  }, {
    value: 'Africa/Nairobi',
    region: 'Africa',
    label: 'Nairobi'
  }, {
    value: 'Africa/Ndjamena',
    region: 'Africa',
    label: 'Ndjamena'
  }, {
    value: 'Africa/Niamey',
    region: 'Africa',
    label: 'Niamey'
  }, {
    value: 'Africa/Nouakchott',
    region: 'Africa',
    label: 'Nouakchott'
  }, {
    value: 'Africa/Ouagadougou',
    region: 'Africa',
    label: 'Ouagadougou'
  }, {
    value: 'Africa/Porto-Novo',
    region: 'Africa',
    label: 'Porto-Novo'
  }, {
    value: 'Africa/Sao_Tome',
    region: 'Africa',
    label: 'Sao Tome'
  }, {
    value: 'Africa/Tripoli',
    region: 'Africa',
    label: 'Tripoli'
  }, {
    value: 'Africa/Tunis',
    region: 'Africa',
    label: 'Tunis'
  }, {
    value: 'Africa/Windhoek',
    region: 'Africa',
    label: 'Windhoek'
  }, {
    value: 'Arctic/Longyearbyen',
    region: 'Arctic',
    label: 'Longyearbyen'
  }, {
    value: 'Asia/Aden',
    region: 'Asia',
    label: 'Aden'
  }, {
    value: 'Asia/Almaty',
    region: 'Asia',
    label: 'Almaty'
  }, {
    value: 'Asia/Amman',
    region: 'Asia',
    label: 'Amman'
  }, {
    value: 'Asia/Aqtau',
    region: 'Asia',
    label: 'Aqtau'
  }, {
    value: 'Asia/Aqtobe',
    region: 'Asia',
    label: 'Aqtobe'
  }, {
    value: 'Asia/Ashgabat',
    region: 'Asia',
    label: 'Ashgabat'
  }, {
    value: 'Asia/Baghdad',
    region: 'Asia',
    label: 'Baghdad'
  }, {
    value: 'Asia/Bahrain',
    region: 'Asia',
    label: 'Bahrain'
  }, {
    value: 'Asia/Baku',
    region: 'Asia',
    label: 'Baku'
  }, {
    value: 'Asia/Bangkok',
    region: 'Asia',
    label: 'Bangkok'
  }, {
    value: 'Asia/Beirut',
    region: 'Asia',
    label: 'Beirut'
  }, {
    value: 'Asia/Bishkek',
    region: 'Asia',
    label: 'Bishkek'
  }, {
    value: 'Asia/Brunei',
    region: 'Asia',
    label: 'Brunei'
  }, {
    value: 'Asia/Choibalsan',
    region: 'Asia',
    label: 'Choibalsan'
  }, {
    value: 'Asia/Colombo',
    region: 'Asia',
    label: 'Colombo'
  }, {
    value: 'Asia/Damascus',
    region: 'Asia',
    label: 'Damascus'
  }, {
    value: 'Asia/Dhaka',
    region: 'Asia',
    label: 'Dhaka'
  }, {
    value: 'Asia/Dili',
    region: 'Asia',
    label: 'Dili'
  }, {
    value: 'Asia/Dubai',
    region: 'Asia',
    label: 'Dubai'
  }, {
    value: 'Asia/Dushanbe',
    region: 'Asia',
    label: 'Dushanbe'
  }, {
    value: 'Asia/Gaza',
    region: 'Asia',
    label: 'Gaza'
  }, {
    value: 'Asia/Hebron',
    region: 'Asia',
    label: 'Hebron'
  }, {
    value: 'Asia/Ho_Chi_Minh',
    region: 'Asia',
    label: 'Ho Chi Minh'
  }, {
    value: 'Asia/Hong_Kong',
    region: 'Asia',
    label: 'Hong Kong'
  }, {
    value: 'Asia/Hovd',
    region: 'Asia',
    label: 'Hovd'
  }, {
    value: 'Asia/Irkutsk',
    region: 'Asia',
    label: 'Irkutsk'
  }, {
    value: 'Asia/Jakarta',
    region: 'Asia',
    label: 'Jakarta'
  }, {
    value: 'Asia/Jayapura',
    region: 'Asia',
    label: 'Jayapura'
  }, {
    value: 'Asia/Jerusalem',
    region: 'Asia',
    label: 'Jerusalem'
  }, {
    value: 'Asia/Kabul',
    region: 'Asia',
    label: 'Kabul'
  }, {
    value: 'Asia/Kamchatka',
    region: 'Asia',
    label: 'Kamchatka'
  }, {
    value: 'Asia/Karachi',
    region: 'Asia',
    label: 'Karachi'
  }, {
    value: 'Asia/Kathmandu',
    region: 'Asia',
    label: 'Kathmandu'
  }, {
    value: 'Asia/Khandyga',
    region: 'Asia',
    label: 'Khandyga'
  }, {
    value: 'Asia/Kolkata',
    region: 'Asia',
    label: 'Kolkata'
  }, {
    value: 'Asia/Krasnoyarsk',
    region: 'Asia',
    label: 'Krasnoyarsk'
  }, {
    value: 'Asia/Kuala_Lumpur',
    region: 'Asia',
    label: 'Kuala Lumpur'
  }, {
    value: 'Asia/Kuching',
    region: 'Asia',
    label: 'Kuching'
  }, {
    value: 'Asia/Kuwait',
    region: 'Asia',
    label: 'Kuwait'
  }, {
    value: 'Asia/Macau',
    region: 'Asia',
    label: 'Macau'
  }, {
    value: 'Asia/Magadan',
    region: 'Asia',
    label: 'Magadan'
  }, {
    value: 'Asia/Makassar',
    region: 'Asia',
    label: 'Makassar'
  }, {
    value: 'Asia/Manila',
    region: 'Asia',
    label: 'Manila'
  }, {
    value: 'Asia/Muscat',
    region: 'Asia',
    label: 'Muscat'
  }, {
    value: 'Asia/Nicosia',
    region: 'Asia',
    label: 'Nicosia'
  }, {
    value: 'Asia/Novokuznetsk',
    region: 'Asia',
    label: 'Novokuznetsk'
  }, {
    value: 'Asia/Novosibirsk',
    region: 'Asia',
    label: 'Novosibirsk'
  }, {
    value: 'Asia/Omsk',
    region: 'Asia',
    label: 'Omsk'
  }, {
    value: 'Asia/Oral',
    region: 'Asia',
    label: 'Oral'
  }, {
    value: 'Asia/Phnom_Penh',
    region: 'Asia',
    label: 'Phnom Penh'
  }, {
    value: 'Asia/Pontianak',
    region: 'Asia',
    label: 'Pontianak'
  }, {
    value: 'Asia/Pyongyang',
    region: 'Asia',
    label: 'Pyongyang'
  }, {
    value: 'Asia/Qatar',
    region: 'Asia',
    label: 'Qatar'
  }, {
    value: 'Asia/Qyzylorda',
    region: 'Asia',
    label: 'Qyzylorda'
  }, {
    value: 'Asia/Rangoon',
    region: 'Asia',
    label: 'Rangoon'
  }, {
    value: 'Asia/Riyadh',
    region: 'Asia',
    label: 'Riyadh'
  }, {
    value: 'Asia/Sakhalin',
    region: 'Asia',
    label: 'Sakhalin'
  }, {
    value: 'Asia/Samarkand',
    region: 'Asia',
    label: 'Samarkand'
  }, {
    value: 'Asia/Seoul',
    region: 'Asia',
    label: 'Seoul'
  }, {
    value: 'Asia/Shanghai',
    region: 'Asia',
    label: 'Shanghai'
  }, {
    value: 'Asia/Singapore',
    region: 'Asia',
    label: 'Singapore'
  }, {
    value: 'Asia/Taipei',
    region: 'Asia',
    label: 'Taipei'
  }, {
    value: 'Asia/Tashkent',
    region: 'Asia',
    label: 'Tashkent'
  }, {
    value: 'Asia/Tbilisi',
    region: 'Asia',
    label: 'Tbilisi'
  }, {
    value: 'Asia/Tehran',
    region: 'Asia',
    label: 'Tehran'
  }, {
    value: 'Asia/Thimphu',
    region: 'Asia',
    label: 'Thimphu'
  }, {
    value: 'Asia/Tokyo',
    region: 'Asia',
    label: 'Tokyo'
  }, {
    value: 'Asia/Ulaanbaatar',
    region: 'Asia',
    label: 'Ulaanbaatar'
  }, {
    value: 'Asia/Urumqi',
    region: 'Asia',
    label: 'Urumqi'
  }, {
    value: 'Asia/Vientiane',
    region: 'Asia',
    label: 'Vientiane'
  }, {
    value: 'Asia/Vladivostok',
    region: 'Asia',
    label: 'Vladivostok'
  }, {
    value: 'Asia/Yakutsk',
    region: 'Asia',
    label: 'Yakutsk'
  }, {
    value: 'Asia/Yekaterinburg',
    region: 'Asia',
    label: 'Yekaterinburg'
  }, {
    value: 'Asia/Yerevan',
    region: 'Asia',
    label: 'Yerevan'
  }, {
    value: 'Atlantic/Azores',
    region: 'Atlantic',
    label: 'Azores'
  }, {
    value: 'Atlantic/Bermuda',
    region: 'Atlantic',
    label: 'Bermuda'
  }, {
    value: 'Atlantic/Canary',
    region: 'Atlantic',
    label: 'Canary'
  }, {
    value: 'Atlantic/Cape_Verde',
    region: 'Atlantic',
    label: 'Cape Verde'
  }, {
    value: 'Atlantic/Faroe',
    region: 'Atlantic',
    label: 'Faroe'
  }, {
    value: 'Atlantic/Madeira',
    region: 'Atlantic',
    label: 'Madeira'
  }, {
    value: 'Atlantic/Reykjavik',
    region: 'Atlantic',
    label: 'Reykjavik'
  }, {
    value: 'Atlantic/South_Georgia',
    region: 'Atlantic',
    label: 'South Georgia'
  }, {
    value: 'Atlantic/St_Helena',
    region: 'Atlantic',
    label: 'St Helena'
  }, {
    value: 'Atlantic/Stanley',
    region: 'Atlantic',
    label: 'Stanley'
  }, {
    value: 'Australia/Adelaide',
    region: 'Australia',
    label: 'Adelaide'
  }, {
    value: 'Australia/Brisbane',
    region: 'Australia',
    label: 'Brisbane'
  }, {
    value: 'Australia/Broken_Hill',
    region: 'Australia',
    label: 'Broken Hill'
  }, {
    value: 'Australia/Currie',
    region: 'Australia',
    label: 'Currie'
  }, {
    value: 'Australia/Darwin',
    region: 'Australia',
    label: 'Darwin'
  }, {
    value: 'Australia/Hobart',
    region: 'Australia',
    label: 'Hobart'
  }, {
    value: 'Australia/Lindeman',
    region: 'Australia',
    label: 'Lindeman'
  }, {
    value: 'Australia/Melbourne',
    region: 'Australia',
    label: 'Melbourne'
  }, {
    value: 'Australia/Perth',
    region: 'Australia',
    label: 'Perth'
  }, {
    value: 'Australia/Sydney',
    region: 'Australia',
    label: 'Sydney'
  }, {
    value: 'Indian/Antananarivo',
    region: 'Indian',
    label: 'Antananarivo'
  }, {
    value: 'Indian/Chagos',
    region: 'Indian',
    label: 'Chagos'
  }, {
    value: 'Indian/Christmas',
    region: 'Indian',
    label: 'Christmas'
  }, {
    value: 'Indian/Cocos',
    region: 'Indian',
    label: 'Cocos'
  }, {
    value: 'Indian/Comoro',
    region: 'Indian',
    label: 'Comoro'
  }, {
    value: 'Indian/Kerguelen',
    region: 'Indian',
    label: 'Kerguelen'
  }, {
    value: 'Indian/Mahe',
    region: 'Indian',
    label: 'Mahe'
  }, {
    value: 'Indian/Maldives',
    region: 'Indian',
    label: 'Maldives'
  }, {
    value: 'Indian/Mauritius',
    region: 'Indian',
    label: 'Mauritius'
  }, {
    value: 'Indian/Mayotte',
    region: 'Indian',
    label: 'Mayotte'
  }, {
    value: 'Indian/Reunion',
    region: 'Indian',
    label: 'Reunion'
  }, {
    value: 'Pacific/Apia',
    region: 'Pacific',
    label: 'Apia'
  }, {
    value: 'Pacific/Auckland',
    region: 'Pacific',
    label: 'Auckland'
  }, {
    value: 'Pacific/Bougainville',
    region: 'Pacific',
    label: 'Bougainville'
  }, {
    value: 'Pacific/Chatham',
    region: 'Pacific',
    label: 'Chatham'
  }, {
    value: 'Pacific/Chuuk',
    region: 'Pacific',
    label: 'Chuuk'
  }, {
    value: 'Pacific/Efate',
    region: 'Pacific',
    label: 'Efate'
  }, {
    value: 'Pacific/Fakaofo',
    region: 'Pacific',
    label: 'Fakaofo'
  }, {
    value: 'Pacific/Fiji',
    region: 'Pacific',
    label: 'Fiji'
  }, {
    value: 'Pacific/Funafuti',
    region: 'Pacific',
    label: 'Funafuti'
  }, {
    value: 'Pacific/Galapagos',
    region: 'Pacific',
    label: 'Galapagos'
  }, {
    value: 'Pacific/Guadalcanal',
    region: 'Pacific',
    label: 'Guadalcanal'
  }, {
    value: 'Pacific/Guam',
    region: 'Pacific',
    label: 'Guam'
  }, {
    value: 'Pacific/Honolulu',
    region: 'Pacific',
    label: 'Honolulu'
  }, {
    value: 'Pacific/Majuro',
    region: 'Pacific',
    label: 'Majuro'
  }, {
    value: 'Pacific/Nauru',
    region: 'Pacific',
    label: 'Nauru'
  }, {
    value: 'Pacific/Niue',
    region: 'Pacific',
    label: 'Niue'
  }, {
    value: 'Pacific/Norfolk',
    region: 'Pacific',
    label: 'Norfolk'
  }, {
    value: 'Pacific/Noumea',
    region: 'Pacific',
    label: 'Noumea'
  }, {
    value: 'Pacific/Pago_Pago',
    region: 'Pacific',
    label: 'Pago Pago'
  }, {
    value: 'Pacific/Palau',
    region: 'Pacific',
    label: 'Palau'
  }, {
    value: 'Pacific/Pitcairn',
    region: 'Pacific',
    label: 'Pitcairn'
  }, {
    value: 'Pacific/Port_Moresby',
    region: 'Pacific',
    label: 'Port Moresby'
  }, {
    value: 'Pacific/Rarotonga',
    region: 'Pacific',
    label: 'Rarotonga'
  }, {
    value: 'Pacific/Saipan',
    region: 'Pacific',
    label: 'Saipan'
  }, {
    value: 'Pacific/Tahiti',
    region: 'Pacific',
    label: 'Tahiti'
  }, {
    value: 'Pacific/Tarawa',
    region: 'Pacific',
    label: 'Tarawa'
  }, {
    value: 'Pacific/Tongatapu',
    region: 'Pacific',
    label: 'Tongatapu'
  }, {
    value: 'Pacific/Wallis',
    region: 'Pacific',
    label: 'Wallis'
  }]);
