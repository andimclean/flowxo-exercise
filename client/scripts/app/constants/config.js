'use strict';

angular
  .module('flowxo-config', [])
  .constant('appConfig', {
    // webpack DefinePlugin is used to convert these into
    // the actual values, see webpack.app.js
    facebookAppId: process.env.FACEBOOK_ID,
    enableAnalytics: process.env.ANALYTICS_ENABLED,
    botWidgetEmbedUrl: process.env.BOT_WIDGET_URL,
    webMessengerShortUrl: process.env.WEB_MESSENGER_SHORT_URL,
  })
;