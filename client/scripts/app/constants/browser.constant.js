'use strict';

angular
  .module('flowxo-core')
  .constant('browser', {
    msie: !!(document.documentMode),
    msedge: /Edge/.test(navigator.userAgent)
  });