'use strict';

// Main logic goes in core, away from routing
angular.module('flowxo-core', [
  'flowxo-config',
  'ngRoute',
  'ui.bootstrap',
  'ui.select',
  'gettext',
  'monospaced.elastic',
  'infinite-scroll',
  'angulartics',
  'angulartics.google.tagmanager',
]);

// Routing goes separately
// so it doesn't interfere with specs
angular
  .module('flowxo-app', [
    'flowxo-core',
    'angular-loading-bar',
  ])

  .provider('notFoundInterceptor', function () {
    this.$get = ['$injector', function ($injector) {
      var $q = $injector.get('$q'),
        $location = $injector.get('$location');

      return {
        responseError: function (response) {
          if (response.status === 404 && response.config.method === 'GET' &&
            /^\/api\/workflows\/[^\/]+$/.test(response.config.url)) {
            // This was a request to GET a single workflow which does
            // not exist. Redirect to the homepage.
            $location.path('/');
          }

          return $q.reject(response);
        }
      };
    }];
  })

  .provider('errorInterceptor', function () {
    this.$get = ['$injector', function ($injector) {
      var $q = $injector.get('$q'),
        NotificationsFactory;

      // Lazy init NotificationsFactory as it is not
      // available during bootstrapping
      var getNotificationsFactory = function () {
        if (!NotificationsFactory) {
          NotificationsFactory = $injector.get('NotificationsFactory');
        }
        return NotificationsFactory;
      };

      return {
        responseError: function (response) {
          var silent = false;

          if (response) {
            // Allow the request to silence error notifications
            if (response.config && response.config.silent) {
              silent = true;

              // Allow the response to silence error notifications
            } else if (response.data && response.data.silent) {
              silent = true;
            }
          }

          if (!silent) {
            getNotificationsFactory().addErrorNotification(response);
          }

          return $q.reject(response);
        }
      };
    }];
  })

  .config(function ($httpProvider, browser) {
    // Add the http interceptors
    $httpProvider.interceptors.push('notFoundInterceptor');

    // Force IE and Edge to not cache http requests
    if (browser.msie && browser.msedge) {
      $httpProvider.defaults.headers.common = {
        'If-Modified-Since': 'Mon, 26 Jul 1997 05:00:00 GMT',
        'Cache-Control': 'no-cache',
        Pragma: 'no-cache'
      };
    }
  })

  .config(function (cfpLoadingBarProvider) {
    // Configure the loading bar
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.latencyThreshold = 500;
  })

  .config(function ($routeProvider, $locationProvider) {
    // Setup the routes
    $routeProvider
      .when('/app', {
        id: 'index',
        controller: 'AppCtrl'
      })
      .when('/app/note', {
        id: 'note',
        controller: 'NoteCtrl'
      })
      .when('/app/note/:note', {
        id: 'note',
        controller: 'NoteCtrl'
      })
      .otherwise({
        redirectTo: '/app'
      });

    $locationProvider.html5Mode(true);
  })

  .run(function ($rootScope) {
    // Figure out which routes are protected.
    // Set the global layout on route change.
    $rootScope.$on('$routeChangeStart', function (ev, next) {

      if (next.id) {
        // Set the template
        next.template = require('views/partials/layouts/' + next.id + '.html');
      }
    });

    $rootScope.$on('$routeChangeSuccess', function (ev, next) {
      if (next.id) {
        // Set the layout so the CSS updates
        $rootScope.layout = next.id;
      }
    });
  });