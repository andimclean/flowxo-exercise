'use strict';

angular
  .module('flowxo-core')
  .controller('AppCtrl', function ($scope, $http) {
    $scope.notes = [];
    
    const SUMMARY_LENGTH = 20;// This should be taken from the config.
    const urlBase = 'http://' + window.location.hostname + ':' + window.location.port + '/';
    $scope.getNotes = function () {

      $http.get(urlBase + 'api/note').then(response => {
        if ('ok' == response.data.result) {
          $scope.notes = response.data.notes;
        }
      })
    };
    $scope.formatDate = function (date) {
      return moment(date).format("YYYY-MM-DD hh:mm:ss");
    };
    $scope.makeSummary = function (text) {
      text = text || '';
      if (text.length > SUMMARY_LENGTH) {
        return text.substring(0, SUMMARY_LENGTH) + "...";
      }
      return text;
    };

    $scope.showNote = function (id) {
      window.location.pathname = "app/note/" + id
    };
    $scope.getNotes();

  });