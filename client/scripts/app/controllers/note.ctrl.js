'use strict';

angular
  .module('flowxo-core')
  .controller('NoteCtrl', function ($scope, $http, $routeParams) {
    const urlBase = 'http://' + window.location.hostname + ':' + window.location.port + '/';
    $scope.id = $routeParams.note;
    $scope.note = {};
    

    $scope.load = function () {
      if ($scope.id) {
        $http.get(urlBase + 'api/note/' + $scope.id).then(response => {
          if ('ok' == response.data.result) {
            $scope.note = response.data.note;
            $scope.showInput = false;
          }
        })
      }
    }

    $scope.formatDate = function (date) {
      return date ? moment(date).format("YYYY-MM-DD hh:mm:ss") : 'unknown';
    };


    $scope.saveNote = function () {
      if ($scope.note.text) {
        if ($scope.id) {
          $http.patch(urlBase + 'api/note/' + $scope.id, {
            text: $scope.note.text
          }).then(response => {
            if ('ok' == response.data.result) {
              $scope.note = response.data.note;              
            }
          })
        } else {
          $http.post(urlBase + 'api/note', {
            text: $scope.note.text
          }).then(response => {
            if ('ok' == response.data.result) {
              $scope.id = response.data.note._id;
              $scope.note = response.data.note;
              $scope.showInput = false;
              window.location.pathname = "app/note/" + $scope.id;
            }
          })
        }
      }
    };

    $scope.cancel = function () {
      window.location.pathname = "app";
    };

    $scope.deleteNote = function() {
      if (confirm("Are you sure you want to delete this note?")) {
        $http.delete(urlBase + 'api/note/' + $scope.id, {
          text: $scope.note.text
        }).then(response => {
          if ('ok' == response.data.result) {            
            window.location.pathname = "/app";
          }
        }).catch(response => {
          console.log(response);
        })
      }
    }
    $scope.load()
  });