;(function($) {
  'use strict';

  // Remove preload class when page has loaded
  // and the DOM has rendered.
  $(function() {
    $('body').removeClass('preload');
  });

})(window.$);