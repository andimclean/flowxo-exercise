;(function(_) {
  'use strict';

  // Pushes each element in `arr` into the current array,
  // returning the current array.
  // http://stackoverflow.com/a/4156156/1171775
  Array.prototype.pushArray = function(arr) {
    this.push.apply(this, arr);
    return this;
  };

  // Adds a new function to lodash to
  // check if something exists
  // (i.e. is not undefined or null).
  /* jshint -W116 */
  /* Allow != null */
  _.exists = function(value) {
    return typeof value != 'undefined' && value != null;
  };
  /* jshint +W116 */

})(window._);
