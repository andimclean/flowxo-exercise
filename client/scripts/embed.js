;(function() {
  'use strict';

  var configProperty = '__FXO';
  var resizeThrottleInterval = 200;

  // Get the config object
  if(!window[configProperty]) {
    window[configProperty] = {
      onResize: null,
      iframes: [],
    };
  }

  var config = window[configProperty];

  if(!config.onResize) {
    window.addEventListener('resize', throttle(onResize, resizeThrottleInterval));
    config.onResize = onResize;
  }

  // Replace the script elements with iframes
  var elements = document.querySelectorAll('.fxo-embed-script');
  [].forEach.call(elements, function(element) {
    var iframe = document.createElement('iframe');
    iframe.className = 'fxo-embed-iframe';
    iframe.scrolling = 'no';
    iframe.src = element.getAttribute('data-src');

    iframe.style = iframe.style || {};
    iframe.style.border = 'none';
    iframe.style.display = 'block';
    iframe.style.width = '100%';

    element.parentNode.replaceChild(iframe, element);

    config.iframes.push(iframe);
  });

  // Call onResize after all of the iframes have been added
  onResize();


  // Inline functions
  function getHeightForWidth(width) {
    return width > 400 ? 80 : 140;
  }

  // Set up the listener
  function onResize() {
    config.iframes.forEach(function(iframe) {
      var dimensions = iframe.getBoundingClientRect();
      var properHeight = getHeightForWidth(dimensions.width);

      if(dimensions.height !== properHeight) {
        iframe.style.height = properHeight + 'px';
      }
    });
  }

  function throttle(fn, time) {
    var waiting = false;
    var timeout;

    return function handler() {
      if(timeout) {
        waiting = true;
        return;
      }

      fn();

      timeout = setTimeout(function() {
        timeout = 0;

        if(waiting) {
          waiting = false;
          handler();
        }
      }, time);
    };
  }
})();