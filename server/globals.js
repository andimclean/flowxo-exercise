'use strict';

var path = require('path');

// Set base rootPath / requireLocal
// http://stackoverflow.com/a/17375258/1171775
// Not ideal but this is the easiest way
// so you can later do
//   requireLocal('path');
// instead of
//   require ('../../../../path');
var serverPath = __dirname;
global.rootPath = path.normalize(serverPath + '/..');

global.requireLocal = function(resource) {
  return require(path.join(serverPath, resource));
};

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
