'use strict';

const noop = function() {
  console.log.apply(console.log, arguments);
};

var createLogger = function() {
  var logger = {
    info: noop,
    debug: noop,
    error: noop,
    warn: noop
  };

  return logger;
};

module.exports = createLogger();
