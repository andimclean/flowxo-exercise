'use strict';

var raven = require('raven');
var Q = require('q');
var logger = requireLocal('logger');

// No args -> use the env var SENTRY_DSN
var client = new raven.Client();
var isEnabled = !!client.dsn;

module.exports = function(err) {
  return Q.Promise(function(resolve, reject) {
    if(!isEnabled || !err) {
      return resolve();
    }

    // Call the callback once we have received a response.
    client.once('logged', function(identity) {
      logger.info('Error sent to Sentry, response:', identity);
      resolve(identity);
    });

    client.once('error', function(e) {
      logger.info('Error could not be sent to Sentry:', e);
      reject(e);
    });

    // Send the error.
    logger.info('Sending error to Sentry:', err);
    client.captureException(err);
  });
};
