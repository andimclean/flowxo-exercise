'use strict';

var crypto = require('crypto');

var CRYPTO_ALGORITHM = 'aes256';
var HASH_ALGORITHM = 'sha256';
var CRYPTO_CLEARTEXT_ENCODING = 'utf8';
var CRYPTO_ENCRYPTED_ENCODING = 'base64';
var IV = 'a2xhcgAAAAAAAAAA';

var CryptoUtils = {};

CryptoUtils.randomBytesAsHexString = function(len, done) {
  crypto.randomBytes(len, (err, buf) => {
    if(err) {
      return done(err);
    }

    done(null, buf.toString('hex'));
  });
};

CryptoUtils.sha1sum = function(val) {
  const shasum = crypto.createHash('sha1');
  shasum.update(val);
  return shasum.digest('hex');
};

/*CryptoUtils.encryptFromString = function(cryptKey, cleartext) {
  var cipher = crypto.createCipher(CRYPTO_ALGORITHM, cryptKey);
  var encrypted = cipher.update(cleartext, CRYPTO_CLEARTEXT_ENCODING, CRYPTO_ENCRYPTED_ENCODING);
  encrypted += cipher.final(CRYPTO_ENCRYPTED_ENCODING);
  return encrypted;
};*/

CryptoUtils.encryptFromString = function(cryptKey, cleartext) {
  var k = crypto.createHash(HASH_ALGORITHM).update(cryptKey).digest();
  var cipher = crypto.createCipheriv(CRYPTO_ALGORITHM, k, IV);

  var encrypted = cipher.update(cleartext, CRYPTO_CLEARTEXT_ENCODING, CRYPTO_ENCRYPTED_ENCODING);
  encrypted += cipher.final(CRYPTO_ENCRYPTED_ENCODING);
  return encrypted;
};

CryptoUtils.encryptFromObject = function(cryptKey, clearObj) {
  return CryptoUtils.encryptFromString(cryptKey, JSON.stringify(clearObj));
};

CryptoUtils.decryptToString = function(cryptKey, encrypted) {
  var k = crypto.createHash(HASH_ALGORITHM).update(cryptKey).digest();
  var decipher = crypto.createDecipheriv(CRYPTO_ALGORITHM, k, IV);
  var decrypted = decipher.update(encrypted, CRYPTO_ENCRYPTED_ENCODING, CRYPTO_CLEARTEXT_ENCODING);
  decrypted += decipher.final(CRYPTO_CLEARTEXT_ENCODING);
  return decrypted;
};

CryptoUtils.decryptToObject = function(cryptKey, encryptedObj) {
  return JSON.parse(CryptoUtils.decryptToString(cryptKey, encryptedObj));
};

module.exports = CryptoUtils;
