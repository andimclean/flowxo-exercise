'use strict';

const _ = require('lodash');
const config = requireLocal('config');

const RATE_LIMIT_COEFFICIENTS = config.bot_rate_limit_coefficients;

module.exports = class PlatformRateLimiter {
  constructor() {
    this.fallbackRunAt = 0;
    this.platformsRunAt = _.reduce(RATE_LIMIT_COEFFICIENTS, (acc, coefficient, key) => {
      acc[key] = {
        coefficient,
        variable: 0
      };
      return acc;
    }, {});
  }

  nextRunAtForPlatform(platform = '') {
    const platformRunAt = this.platformsRunAt[platform];
    if(!platformRunAt) {
      return this.fallbackRunAt += 100;
    }
    return platformRunAt.coefficient * platformRunAt.variable++;
  }
};
