'use strict';

var _ = require('lodash'),
    numeral = require('numeral'),
    xml2json = require('xml2json'),
    stringSimilarity = require('string-similarity'),
    diacritics = require('diacritics'),
    FxoUtils = require('flowxo-utils'),
    config = requireLocal('config');

const MATCH_MIN_CONFIDENCE = config.requests.keyword_matching_min_confidence;

const DataUtils = {};

// Match a series of {{ <24-char-hex>.<property> <options> <optionValue> }} in a string
const INTERPOLATE_REGEX = /{{\s*([a-f0-9]{24})\.([^\s}]+)\s*(n|=|#|&&|&|\d+|\*|>)?\s*([^\s}]*)?\s*}}/g;

const MISSING_INTERPOLATION_PLACEHOLDER = '{{ MISSING OUTPUT }}';

const DEFAULT_INTERPOLATION_OPTION = 1;

const OBJECT_DELIMITER = '__';
const COLLECTION_DELIMITER = '_+_';
const COLLECTION_OPTION_REGEX = /n|=|#|&&|&|\d+|\*/;

// Message grouping eg. 'Your opening time [hours]'
const GROUP_REGEX_STR = '\\s*\\[([^\\[\\]]+)\\]\\s*';

/*jshint eqnull: true */
DataUtils._getWithFlattenedKey = function(key, data, options, optionValue) {
  var fallbackRtn = '',
      collectionInnerKeys;

  // Don't continue if we have no request data to lookup
  if(!data) {
    return fallbackRtn;
  }

  var opts = {
    // Create a formatter for the collections
    arrayFormatter(keys, outputData, innerKeys) {
      outputData = outputData || data;
      let result = DataUtils._getCollectionFormatter(options)(keys, outputData, innerKeys);
      if(!innerKeys || !innerKeys.length) {
        return result;
      }

      // If the collection contains inner arrays
      collectionInnerKeys = _.clone(innerKeys.reverse());
      let objResult = {};
      innerKeys.reduceRight((prev, curr, index) => {
        if(!index) {
          prev[curr] = result;
        } else {
          prev[curr] = {};
        }
        return prev[curr];
      }, objResult);
      return objResult;
    }
  };

  // Lookup the value, accounting for the fact that it
  // might be a flattened key
  var val = FxoUtils.getFlattenedValue(data, key, opts);

  if(Array.isArray(val) && collectionInnerKeys) {
    val = val.map(item => {
      for(let i = 0; i < collectionInnerKeys.length; i++) {
        if(item[collectionInnerKeys[i]]) {
          item = item[collectionInnerKeys[i]];
        } else {
          break;
        }
      }
      return item;
    });
  }

  // Special operations in non-collection objects
  switch (options) {
    case '>':
      if(optionValue) {
        let data = DataUtils.strToObject(val) || {};
        val = DataUtils._getWithFlattenedKey(optionValue.trim(), data);
      }
      break;
  }

  // Stringify objects
  if(_.isPlainObject(val)) {
    val = JSON.stringify(val);
  }

  // Preserve empty strings, number 0, boolean false
  // by comparing value `!= null`
  return val != null ? val : fallbackRtn;
};
/* jshint eqnull: false */

DataUtils._getCollectionFormatter = function(options) {

  var getValueFromKeys = function (keys, data) {
    return keys.reduce((prev, key) => {
      if(prev) {
        return prev[key];
      }
      return null;
    }, data);
  };

  return function(keys, data, innerKeys) {
    if(Array.isArray(innerKeys)) {
      keys = keys.concat(innerKeys.reverse());
    }

    if(!data || !Array.isArray(data)) {
      data = [];
    }

    options = options || DEFAULT_INTERPOLATION_OPTION;

    data = data
      .map(item => getValueFromKeys(keys, item))
      .filter(item => item);

    switch (options) {
      // Return last item
      case 'n':
        return data[data.length - 1];

      // Add values
      case '=':
        var result = data.reduce((prev, obj) => prev + numeral(obj).value(), 0);
        return numeral(result).format('0[.]00');

      // Count values
      case '#':
        return data.length;

      // Space separated list of all values
      case '&':
        return data.reduce((prev, obj) => prev + ' ' + obj, '').trim();

      // Formatted list of all values
      case '&&':
        return data.reduce((prev, obj, i) => {
          if(!i) {
            return obj;
          }
          if(i === data.length - 1) {
            return prev + ' & ' + obj;
          }
          return prev + ', ' + obj;
        }, '');

      // Magic Repeat (returns an array)
      case '*':
        return data;
    }

    // Handle numbers
    return data[parseInt(options) - 1] || '';
  };
};

/**
 * Validates any interpolations in the passed string.
 * Validation is governed by the matcherFn.
 * A valid interpolation will be preserved, whereas
 * an invalid one will be replaced by a placeholder.
 * @param  {String} str       string containing interpolations
 * @param  {Function} matcherFn function to test interpolation validity. Return truthy to preserve interpolation, falsy to replace interpolation with a placeholder.
 * @return {String}           string with interpolations preserved or replaced
 */
DataUtils.validateInterpolations = function(str, matcherFn) {
  if(!_.isString(str)) { return str; }

  return str.replace(INTERPOLATE_REGEX, function(match, id, key, options, optionValue) {
    let baseKey = key.split(OBJECT_DELIMITER)[0]
                     .split(COLLECTION_DELIMITER)[0];
    var isValid = matcherFn({
      expr: match,
      id: id,
      key: key,
      baseKey,
      options,
      optionValue
    });

    return isValid ? match : MISSING_INTERPOLATION_PLACEHOLDER;
  });
};

DataUtils.validInterpolation = function(str) {
  return INTERPOLATE_REGEX.test(str);
};

DataUtils.includesMissingPlaceholder = function(str) {
  if(!_.isString(str)) {
    // It can't possibly contain the placeholder
    // if it isn't a string.
    return false;
  }

  return str.includes(MISSING_INTERPOLATION_PLACEHOLDER);
};

// Look for one or more `{{ id.key }}`,
// parsing out any leading or trailing whitespace.
DataUtils.getInterpolatedValue = function(str, requestData) {
  if(!_.isString(str)) {
    return str;
  }

  var repeats = [];
  var interpolateTexts = [];
  var firstCollection;
  var result = str.replace(INTERPOLATE_REGEX, function(text, id, key, options, optionValue) {
    // For a interpolation expression {{ id.key > 5 }}
    //  - text: `{{ id.key }}`
    //  - id: `id`
    //  - key: `key`
    //  - options: `>`
    //  - optionValue: `5`

    // Only repeat items from the first collection
    if(options && options === '*') {
      if(!firstCollection) {
        firstCollection = key;
      } else if(!DataUtils._sameCollection(firstCollection, key)) {
        options = '';
      }
    }

    // If the option is for collections, force the output to be a collection
    if(options && COLLECTION_OPTION_REGEX.test(options) && key.indexOf(COLLECTION_DELIMITER) === -1) {
      key = DataUtils.getCollectionKey(key);
    }

    var data = requestData && requestData[id];
    var replace = DataUtils._getWithFlattenedKey(key, data, options, optionValue);

    if(!Array.isArray(replace) || options !== '*') {
      if(_.isObject(replace) && !_.isDate(replace)) {
        try {
          return JSON.stringify(replace);
        } catch(e) {}
      }
      return replace;
    }

    // Prepare to repeat the collection
    repeats.push(replace);
    interpolateTexts.push(text);
    return text;
  });

  if(!repeats.length) {
    return result;
  }

  return DataUtils._doCollectionRepeats(result, repeats, interpolateTexts);
};

DataUtils._doCollectionRepeats = function(str, repeats, interpolateTexts) {
  let iterations = repeats.reduce((prev, curr) => {
    if(curr.length > prev) {
      return curr.length;
    }
    return prev;
  }, 0);

  let repeatLine = function(i) {
    return repeats.reduce((prev, curr, index) => {
      return prev.replace(interpolateTexts[index], curr[i] || '');
    }, str);
  };

  let repeaterText = '';
  for(let i = 0; i < iterations; i++) {
    repeaterText += repeatLine(i);
    if(i < iterations - 1) {
      repeaterText += '\n';
    }
  }

  return repeaterText;
};

DataUtils._sameCollection = function(collection1, collection2) {
  var parts1 = collection1.split(COLLECTION_DELIMITER);
  var parts2 = collection2.split(COLLECTION_DELIMITER);
  var parent;
  var coll;
  if(parts1.length <= 1 || parts2.length <= 1) {
    return true;
  }
  if(parts1.length > parts2.length) {
    parent = parts2[parts2.length - 2];
    coll = parts1;
  } else {
    parent = parts1[parts1.length - 2];
    coll = parts2;
  }
  for(let i = 0; i < coll.length; i++) {
    if(coll[i] === parent) {
      return true;
    }
  }
  return false;
};

/**
 * Replaces the last "__\d+__" with the collection delimiter
 */
DataUtils.getCollectionKey = function(key) {
  return key.replace(/__\d+__(?!.*__\d+__)/, COLLECTION_DELIMITER);
};

DataUtils.composeGetInterpolatedValueFn = function(requestData) {
  return function(str) {
    return DataUtils.getInterpolatedValue(str, requestData);
  };
};

/**
 * Parses any JSON or XML into an object
 */
DataUtils.strToObject = function(str) {
  try {
    str = str.trim();
    if(str.startsWith('<')) {
      str = xml2json.toJson(str);
    }
    return JSON.parse(str);
  } catch(e) {
    return '';
  }
};

DataUtils.parseRegExp = function(string) {
  let testRegex = /^\/.*\/([gimy]*)$/;
  let regexp;

  // Using a try-catch since we don't know if the string will be a valid regex
  try {
    if(testRegex.test(string)) {
      let flags = string.replace(testRegex, '$1');
      let pattern = string.replace(new RegExp(`^/(.*?)/${flags}$`), '$1');
      regexp = new RegExp(pattern, flags);
    } else {
      regexp = new RegExp(string);
    }
  } catch(e) {
    return null;
  }

  return regexp;
};

DataUtils.buildInputsWithRequestData = function(inputData, requestData, options = {}) {
  const isArray = _.isArray(inputData);
  const isPlainObject = _.isPlainObject(inputData);
  const { timezone, locale } = options;
  if(!(isArray || isPlainObject)) {
    return inputData;
  }

  function interpolate(val) {
    return DataUtils.getInterpolatedValue(val, requestData);
  }

  return _.reduce(inputData, (hash, val, key) => {
    // Interpolate any linked data values
    // Account for both key=value and [{ key: 'key', value: 'value '}]

    let type;
    if(isArray) {
      key = val && val.key;
      type = val && val.type;
      val = val && val.value;
    }

    // Only set if the value isn't null, undefined or empty
    /* jshint eqnull: true */
    if(val != null && val !== '') {
    /* jshint eqnull: false */
      // Handle special fields
      switch(type) {
        case 'datetime':
          val = FxoUtils.parseDateTimeField(interpolate(val), {timezone, locale});
          break;

        case 'boolean':
          val = FxoUtils.parseBooleanField(interpolate(val));
          break;

        case 'dictionary':
          // Reduce into a JSON object.
          // If the key is already used,
          // or is blank, ignore.
          val = _.reduce(val, function(hash, item) {
            // Interpolate both the key and value.
            let key = interpolate(item.key);
            if(key && !(key in hash)) {
              hash[key] = interpolate(item.value);
            }
            return hash;
          }, {});
          break;

        default:
          val = interpolate(val);
          break;
      }

      // Only set if key is not blank,
      // and not already used.
      if(key && !(key in hash)) {
        hash[key] = val;
      }
    }

    return hash;
  }, {});
};

/**
 * Finds the best matching option if it's above the minimum confidence
 */
DataUtils.findMatchIndex = function(text, options, hooks) {
  options = options || [];

  // Clean data to reduce noise
  let cleanText = DataUtils._cleanKeyword(text, hooks);
  let cleanOptions = options.map(opt => DataUtils._cleanKeyword(opt, hooks));

  // If the text is just one character, find any exact match
  if(cleanText.length <= 1) {
    return cleanOptions.indexOf(cleanText);
  }

  let matches = stringSimilarity.findBestMatch(cleanText, cleanOptions);
  if(!matches || !matches.bestMatch || !matches.bestMatch.rating) {
    return -1;
  }

  // If there are 2 or more options with the best match, we cannot know the right answer
  if(matches.ratings && matches.ratings.length) {
    let totalBestMatches = matches.ratings
      .find(match => match.rating === matches.bestMatch.rating)
      .length;
    if(totalBestMatches > 1) {
      return -1;
    }
  }

  // Numbers require an exact match
  let isNumber = !Number.isNaN(Number(cleanText));
  let minConfidence = isNumber ? 1 : MATCH_MIN_CONFIDENCE;

  // If the confidence is above the minimum return the index of the match
  if(matches.bestMatch.rating >= minConfidence) {
    return cleanOptions.indexOf(matches.bestMatch.target);
  }

  return -1;
};

DataUtils._cleanKeyword = function(keyword, hooks = {}) {
  if(hooks.beforeClean) {
    keyword = hooks.beforeClean(keyword);
  }

  keyword = diacritics.remove(keyword)

    // Replace unnecessary characters by white spaces
    .replace(/[\\/!?.,:;\-()\[\]£$@]/g, ' ')

    // Remove quotes
    .replace(/["'`]/g, '')

    // Remove consecutive whitespaces
    .replace(/\s+/g, ' ')

    .toLowerCase()
    .trim();

  // Join numbers separated by spaces
  let parts = keyword.split(' ');
  keyword =  parts.reduce((prev, curr, index) => {
    if(!prev) {
      return curr;
    }
    if(!Number.isNaN(Number(parts[index-1])) && !Number.isNaN(Number(curr))) {
      return prev + curr;
    }
    return prev + ' ' + curr;
  }, '');

  if(hooks.afterClean) {
    keyword = hooks.afterClean(keyword);
  }

  return keyword;
};

DataUtils.containsPhrase = function(phrase, body) {
  // Checks to see if the passed phrase occurs
  // inside the body of text.
  // The phrase must match completely, with special
  // characters considered to be word boundaries.
  phrase = DataUtils._normaliseWordBoundaryChars(phrase);
  body = DataUtils._normaliseWordBoundaryChars(body);

  var matcher = new RegExp(`(?:^|\\s+)${_.escapeRegExp(phrase)}(?:$|\\s+)`, 'i');
  return matcher.test(body);
};

DataUtils._normaliseWordBoundaryChars = function(str) {
  return str
    .replace(/[\s!?.,<>'"`():;{}[\]]+/g, ' ')
    .trim();
};

DataUtils._findGroupIndex = function(str) {
  const regex = new RegExp(GROUP_REGEX_STR, 'g');

  let match, matches = [];
  while ((match = regex.exec(str)) !== null) {
    matches.push(match.index);
  }

  // Return last match
  return matches[matches.length - 1];
};

DataUtils.stripGroup = function(str) {
  const idx = DataUtils._findGroupIndex(str);

  if(idx) {
    return str.substring(0, idx);
  } else {
    return str;
  }
};

DataUtils.extractGroupValue = function(str) {
  const idx = DataUtils._findGroupIndex(str);

  if(idx) {
    let result = str.substring(idx).match(new RegExp(GROUP_REGEX_STR));
    return result && result[1];
  } else {
    return null;
  }
};

DataUtils.isJSON = function(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};

module.exports = DataUtils;
