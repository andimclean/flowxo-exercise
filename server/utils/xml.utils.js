'use strict';

var xml2js = require('xml2js'),
    xmlParseOptions = requireLocal('config').xml_parse_options,
    xmlBuildOptions = requireLocal('config').xml_build_options;

var XmlUtils = {};

XmlUtils.parse = function(xmlStr, done) {
  new xml2js.Parser(xmlParseOptions)
    .parseString(xmlStr, done);
};

XmlUtils.build = function(obj) {
  return new xml2js.Builder(xmlBuildOptions)
    .buildObject(obj);
};

module.exports = XmlUtils;
