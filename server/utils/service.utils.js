'use strict';

var services = requireLocal('services');

var ServiceUtils = module.exports = {};

ServiceUtils.requireInternal = function(slug) {
  return services.internal(slug);
};

ServiceUtils.requireExternal = function(slug) {
  return services.external(slug);
};

ServiceUtils.requireIntegration = function(slug) {
  return services.integration(slug);
};

ServiceUtils.require = function(s) {
  if(s.internal) {
    return ServiceUtils.requireInternal(s.slug);
  } else if(s.integration) {
    return ServiceUtils.requireIntegration(s.slug);
  }
  return ServiceUtils.requireExternal(s.slug);
};

/**
 * @return {IntegrationClient}
 */
ServiceUtils.getIntegrationClient = function(slug, credentials) {
  var clientClass = services.integrationClient(slug);
  if(clientClass) {
    return new clientClass(credentials);
  }
  return null;
};

ServiceUtils.getIntegrationClientClass = function(slug) {
  return services.integrationClient(slug);
};
