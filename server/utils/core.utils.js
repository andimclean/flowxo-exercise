'use strict';

const mongoose = require('mongoose');
const _ = require('lodash');

const CoreUtils = {};

CoreUtils.generateUID = function() {
  // Use MongoDB's ObjectID.
  // The only way a collision could happen:
  //  - Generation occurs on the same machine, or a different machine
  //    which generated the same machine ID, and
  //  - Generation happens in the same process, or a different
  //    process which has/generated the same process ID, and
  //  - The ObjectID is generated at the same time (in seconds), and
  //  - The counter is at the same value
  // This is so, so unlikely that it is safe to use ObjectIDs.
  // If we do happen to collide, we could look at using UUIDs instead.
  return mongoose.Types.ObjectId().toString();
};

/* jshint eqnull: false, expr: false */

CoreUtils.getRandomSample = function(items) {
  return _.sample(items);
};

module.exports = CoreUtils;
