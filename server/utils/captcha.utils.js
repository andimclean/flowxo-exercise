'use strict';

var request = require('request'),
    config = requireLocal('config');

var CaptchaUtils = module.exports = {};

/**
 * Checks if the user requires a captcha to continue with the login process
 *
 * @param user
 * @returns {boolean}
 */
CaptchaUtils.loginRequiresCaptcha = function(user) {
  return process.env.GOOGLE_RECAPTCHA_ID &&
    user.login_attempts > config.show_captcha.login_attempts;
};

CaptchaUtils.globalActionRequiresCaptcha = function(doc) {
  return process.env.GOOGLE_RECAPTCHA_ID &&
    doc.actions > config.show_captcha.global_actions;
};

CaptchaUtils.verifyCaptcha = function(captcha, ipAddress, done) {
  var opt = {
    url: 'https://www.google.com/recaptcha/api/siteverify',
    method: 'POST',
    json: true,
    form: {
      secret: process.env.GOOGLE_RECAPTCHA_SECRET,
      response: captcha,
      remoteip: ipAddress
    }
  };
  request.post(opt, function(err, res, body) {
    if(err) {
      return done(err);
    }
    done(null, body.success === true);
  });
};
