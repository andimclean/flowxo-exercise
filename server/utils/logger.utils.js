'use strict';

var logger = requireLocal('logger'),
    config = requireLocal('config').custom_log_levels;

const noop = function() {};

const cache = {};

const getCachedLogger = function(id) {
  if(!cache[id]) {
    cache[id] = logger.createNewLogger({
      logLabel: id,
      level: config[id]
    });
  }

  return cache[id];
};

var LoggerUtils = {};

LoggerUtils.getRequestLogger = function(data) {
  var labelParts = [];
  if(data) {
    if(data.mid) {
      labelParts.push('m:' + data.mid);
    }
    if(data.rid) {
      labelParts.push('r:' + data.rid);
    }
  }

  return logger.createNewLogger({
    logLabel: labelParts.join(' '),
    level: config.Request
  });
};

LoggerUtils.getHeartbeatLogger = function() {
  return getCachedLogger('Heartbeat');
};

LoggerUtils.getBotmasterCommsLogger = function() {
  return getCachedLogger('BMComms');
};

LoggerUtils.getWorkflowJobLogger = function() {
  return getCachedLogger('WorkflowJob');
};

const logDebug = function(msg, data) {
  logger.debug(msg, data);
};

LoggerUtils.logEmailDebug = +process.env.EMAILER_DEBUG_ENABLED ?  logDebug : noop;

module.exports = LoggerUtils;
