'use strict';

var util = require('util'),
    async = require('async'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    logger = requireLocal('logger'),
    HashID = requireLocal('utils/hashid'),
    ErrorUtils = require('./error.utils');

// Match ISO dates with or without milliseconds
// 2013-09-26T03:05:26.000Z
// 2013-09-26T03:05:26Z
// This is a pretty lax regex, it doesn't check that
// days/months/hours/mins/secs are in the correct bounds.
// For this reason, we check for an invalid date after
// applying a matching string to `new Date()`.
const ISO_8601_DATE_FORMAT = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d{3})?Z$/;

var DBUtils = module.exports = {};

DBUtils.isValidObjectId = function(id) {
  return id instanceof ObjectId || ObjectId.isValid(id);
};

// The first 4 bytes of an ObjectID is the time when it was created.
// We can use this along with a pad of 0's to query the ID field
// by time, thus getting values <, > etc wrt a desired timestamp.
// Credit to http://stackoverflow.com/a/8753670/1171775
DBUtils.objectIdWithTimestamp = function(timestamp) {
  // Convert date object to hex seconds since Unix epoch
  var hexSeconds = Math.floor(timestamp/1000).toString(16);

  // Return an ObjectId with that hex timestamp
  return ObjectId(hexSeconds + '0000000000000000');
};

DBUtils.hasValidationErrors = function(doc) {
  return doc.errors && doc.errors.length > 0;
};

DBUtils.isMongooseCastError = function(err, path) {
  var isCastError = err && err.name;

  if(isCastError) {
    // Check the path
    if(util.isArray(path)) {
      // Check if the error path is in the passed array.
      isCastError = (path.indexOf(err.path) !== -1);

    } else if(path) {
      isCastError = err.path === path;
    }
  }

  return isCastError;
};

DBUtils.isMongooseInvalidIDError = function(err) {
  return DBUtils.isMongooseCastError(err, '_id');
};

DBUtils.isMongooseValidationError = function(err) {
  return err && err.name === 'ValidationError';
};

DBUtils.isMongoUniqueKeyViolationError = function(err, propName) {
  let matches = err && err.message && err.message.includes('E11000 duplicate key error');
  if(matches && propName) {
    matches = err.message.includes(propName);
  }
  return matches;
};

DBUtils.buildValidationError = function(msg) {
  return {
    name: 'ValidationError',
    msg: msg
  };
};

DBUtils.executeSingleDocQuery = function(query, user, cb) {
  query.exec(function(err, doc) {
    err = DBUtils.checkSingleDocDBResponse(err, doc, user);
    cb(err, doc);
  });
};

// Returns an error object if there is an error, or null if all OK.
DBUtils.checkSingleDocDBResponse = function(err, doc, user) {
  if(err) {
    return err;
  }

  if(!doc) {
    // No doc in the database.
    return ErrorUtils.get404NotFoundError();
  }

  if(user && (!doc.user || !doc.user.equals(user._id))) {
    // This doc belongs to somebody else.
    return ErrorUtils.get403ForbiddenError();
  }

  return null;
};

DBUtils.executeMultiDocQuery = function(query, user, cb) {
  query.exec(function(err, docs) {
    err = DBUtils.checkMultiDocDBResponse(err, docs, user);
    cb(err, docs);
  });
};

// Returns an error object if there is an error, or null if all OK.
DBUtils.checkMultiDocDBResponse = function(err, docs, user) {
  if(err) {
    return err;
  }

  for(let key in docs) {
    if(docs.hasOwnProperty(key)) {
      if(user && (!docs[key].user || !docs[key].user.equals(user._id))) {
        // This doc belongs to somebody else.
        return ErrorUtils.get403ForbiddenError();
      }
    }
  }

  return null;
};

DBUtils.validateSlug = function(value) {
  // Ensure it is a non-empty valid slug
  return (/^[a-z0-9-_]+$/).test(value);
};

var jsonReviver = function(key, val) {
  // Revive Dates correctly.
  if(!_.isString(val)) {
    return val;
  }

  if(!ISO_8601_DATE_FORMAT.test(val)) {
    return val;
  }

  var ts = Date.parse(val);
  if(_.isNaN(ts)) {
    return val;
  }

  return new Date(ts);
};

var parseJSON = function(json) {
  // In a separate function as it can't
  // be optimised by v8.
  // http://stackoverflow.com/a/29798770/1171775
  try {
    return JSON.parse(json, jsonReviver);
  } catch (e) {
    return json;
  }
};

DBUtils.jsonGetter = function(data) {
  return _.isString(data) ? parseJSON(data) : data;
};

DBUtils.jsonSetter = function(data) {
  return JSON.stringify(data);
};

DBUtils.jsonType = function() {
  return {
    type: mongoose.Schema.Types.Mixed,
    get: DBUtils.jsonGetter,
    set: DBUtils.jsonSetter
  };
};

DBUtils.asPaginatedResponse = function(options, cb) {
  const workObj = {};

  workObj[options.recordsKey] = function(cb) {
    options.docQuery
      .skip(options.offset)
      .limit(options.limit)
      .exec(cb);
  };

  workObj.total = function(cb) {
    options.countQuery.exec(cb);
  };

  workObj.offset = function(cb) {
    cb(null, options.offset);
  };

  workObj.limit = function(cb) {
    cb(null, options.limit);
  };

  async.parallel(workObj, cb);
};

DBUtils.generateShortIdAndSave = function(doc, propName, done) {
  const limit = 50;
  let attempts = 0;

  // Begin the recursion, and account for callbacks
  return create().nodeify(done);

  function create() {
    attempts++;
    if(attempts > 1) {
      logger.debug(`Create unique, attempt ${attempts} of ${limit}`);
    }

    doc[propName] = HashID.generate();
    return doc.save()
      .then(doc => {
        // We have created the doc successfully, return it.
        logger.debug(`created with unique id: ${doc[propName]}`);
        return doc;
      })
      .catch(err => {
        // If there was a unique constraint due to a
        // duplicate friendly ID, we should try again.
        if(DBUtils.isMongoUniqueKeyViolationError(err, propName)) {
          if(attempts < limit) {
            // Good to go again.
            return create();
          }

          // Otherwise, we have hit the limit.
          throw new Error(`Could not generate unique ${propName}`);
        }

        // Otherwise, if there was any other error, throw it
        throw err;
      });
  }
};