'use strict';

var bcrypt = require('bcrypt'),
    config = requireLocal('config'),
    FxoUtils = require('flowxo-utils');

var commonPasswords = FxoUtils.toHash(config.user_passwords.most_common_list);

var PasswordUtils = module.exports = {};

/**
 * Checks if the password is too short
 *
 * @param {string} password
 * @returns {boolean}
 */
PasswordUtils.isTooShort = function(password) {
  if(!password) {
    return true;
  }
  return password.length < config.user_passwords.min_length;
};

/**
 * Checks if the password is too long
 *
 * @param {string} password
 * @returns {boolean}
 */
PasswordUtils.isTooLong = function(password) {
  if(!password) {
    return false;
  }
  return password.length > config.user_passwords.max_length;
};

PasswordUtils.isTooCommon = function(password) {
  return !!commonPasswords[password];
};

PasswordUtils.getPasswordHash = function(password, done) {
  bcrypt.genSalt(config.user_passwords.salt_work_factor, function(err, salt) {
    if(err) {
      return done(err);
    }
    bcrypt.hash(password, salt, function(err, hash) {
      if(err) {
        return done(err);
      }
      done(null, hash);
    });
  });
};

PasswordUtils.validatePassword = bcrypt.compare;
