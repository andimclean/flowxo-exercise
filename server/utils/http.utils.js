'use strict';

var request = require('request');

var HttpUtils = {};

HttpUtils.makeRequest = function(opts, done) {
  // So we can stub out `request` in unit testing.
  request(opts, done);
};

module.exports = HttpUtils;
