'use strict';

var request = require('request'),
    config = requireLocal('config'),
    logger = requireLocal('logger'),
    FxoUtils = require('flowxo-utils');

var DateUtils = {};

DateUtils._makeRequest = function(opts, done) {
  // So it can be faked for testing.
  request(opts, done);
};

DateUtils.getTimezoneFromIPAddress = function(ipAddress, done) {
  if(!config.allow_timezone_lookup) {
    return done(new Error('Server configuration has prevented a timezone lookup.'));
  }

  if(!ipAddress) {
    return done(new Error('IP Address not specified.'));
  }

  var opts = {
    url: config.maxmind.url + '/' + ipAddress,
    auth: {
      username: config.maxmind.username,
      password: config.maxmind.license_key
    },
    json: true
  };

  DateUtils._makeRequest(opts, function(err, response, body) {
    if(err) { return done(err); }
    if(response.statusCode > 299) {
      var msg = body ? (body.error || body) : 'Unknown error';
      return done(new Error(msg));
    }

    done(null, body && body.location && body.location.time_zone);
  });
};

var pluralise = function(token, item) {
  return item + ' ' + token + (item > 1 ? 's' : '');
};

/**
 * Convert millisecs into a string like
 * 5 days, 4 hours, 30 minutes, 15 seconds
 * @param  {Number} millis milliseconds
 * @return {String}        timeAgo string.
 */
DateUtils.timeAgo = function(millis) {
  var seconds = Math.floor(millis / 1000);
  if(seconds <= 0) {
    return 'now';
  }

  var parts = [];

  var days = Math.floor(seconds / 86400);
  if(days > 0) {
    parts.push(pluralise('day', days));
    seconds = seconds - (days * 86400);
  }

  var hours = Math.floor(seconds / 3600);
  if(hours > 0) {
    parts.push(pluralise('hour', hours));
    seconds = seconds - (hours * 3600);
  }

  var minutes = Math.floor(seconds / 60);
  if(minutes > 0) {
    parts.push(pluralise('minute', minutes));
    seconds = seconds - (minutes * 60);
  }

  seconds = Math.floor(seconds);
  if(seconds > 0) {
    parts.push(pluralise('second', seconds));
  }

  return parts.join(', ');
};

/**
 * DateUtils.parseDate - Parses a date using DateUtils.parse but returns a
 * string formatted `yyyy-mm-dd`
 *
 * @param  {String} dateStr      a string that can be converted into a date using SugarDate
 * @param  {Object} options = {} See DateUtils.parse
 * @return {String}              `yyyy-mm-dd`
 */
DateUtils.parseDate = function(...args) {
  return DateUtils.parse(...args).format('YYYY-MM-DD');
};

/**
 * DateUtils.parseTime - Parses a time using DateUtils.parse but returns a
 * string formatted `hh:mm:ss`
 *
 * @param  {String} dateStr      a string that can be converted into a date using SugarDate
 * @param  {Object} options = {} See DateUtils.parse
 * @return {String}              `hh:mm:ss`
 */
DateUtils.parseTime = function(...args) {
  return DateUtils.parse(...args).format('HH:mm:ss');
};

/**
 * DateUtils.parseDateTime - Parses a datetime using DateUtils.parse but returns a
 * string formatted `yyyy-mm-ddThh:mm:ss-off:set`
 *
 * @param  {String} dateStr      a string that can be converted into a date using SugarDate
 * @param  {Object} options = {} See DateUtils.parse
 * @return {String}              `yyyy-mm-ddThh:mm:ss-off:set`
 */
DateUtils.parseDateTime = function(...args) {
  return DateUtils.parse(...args).format('YYYY-MM-DDTHH:mm:ssZ');
};

/**
 * DateUtils.parse - Parses and creates a date in a specific timezone and is locale aware
 *
 * @param  {String} dateStr a string that can be converted into a date using SugarDate
 * @param  {Object} options timezone: string Ex, `America/Chicago`, defaults 'UTC'
 * @param  {Object} options locale: string Ex, `en-GB`, guesses from timezone, defaults to 'en'
 * @return {Moment}         moment.js object
 */
DateUtils.parse = function(dateStr, options = {}) {
  logger.debug(`DateUtils.parse: ${dateStr}`, options);

  // Parse string into date using FxoUtils.parseDateTimeField. Ex, 'tomorrow at 6pm'
  let fxoDate = FxoUtils.parseDateTimeField(dateStr, options);

  if (!fxoDate.valid) {
    throw new TypeError('Date is not valid');
  }

  // Returns a Moment object.
  // .format() for standard datetime format Ex, `2017-01-17T19:00:00-05:00`
  // .toISOString() for ISO datetiem format Ex, '2017-01-18T00:00:00.000Z'
  // See http://momentjs.com/docs/ for more
  return fxoDate.moment;
};

module.exports = DateUtils;
