'use strict';

var qs = require('querystring'),
    _ = require('lodash'),
    Q = require('q'),
    passport = require('passport'),
    refresh = require('passport-oauth2-refresh'),
    logger = requireLocal('logger'),
    User = requireLocal('models/user.model'),
    oauth = require('oauth'),
    request = require('request');

var AuthUtils = module.exports = {};

AuthUtils.buildExternalAuthCallbackURL = function(base, provider, redirectId) {
  let callbackUrl = base + provider;
  if(redirectId) {
    callbackUrl += `/${redirectId}`;
  }
  return callbackUrl + '/callback';
};

AuthUtils.passportAuthorize = function(req, res, next, strategy, authOptions, callback) {
  passport.authorize(strategy, authOptions, callback)(req, res,next);
};

AuthUtils.renderAuthCompleteView = function(res, data) {
  // Transform into an object
  if(data) {
    data = { data: data };
  }

  return res.render('authcomplete', data);
};

AuthUtils.renderAuthErrorView = function(res, err, errorMsg, showErrReason) {
  if(err) {
    logger.warn('Auth Error', err);
  }

  return res.render('autherror', {
    errorMsg: errorMsg || 'There was a problem when trying to connect with this service.',
    errReason: (showErrReason && err) ? err.toString() : null
  });
};

AuthUtils.buildExternalAuthResponseCallback = function(res, done) {
  return function(err, result) {
    if(err) {
      // Render the autherror view.
      return AuthUtils.renderAuthErrorView(res, err);
    }

    if(!result) {
      // Render the authcomplete view with no data.
      // The front-end should recognise there is no data
      // and allow the user to try again.
      return AuthUtils.renderAuthCompleteView(res);
    }

    done(result);
  };
};

AuthUtils.prefixLoginStrategy = function(name) {
  return 'login-' + name;
};

AuthUtils.prefixServiceStrategy = function(name) {
  return 'service-' + name;
};

AuthUtils.useLocalStrategy = function(Strategy, options) {
  var strategy = new Strategy(options, function(req, email, password, done) {
    User.verifyLocalLogin(email, password, req.validCaptcha, done);
  });
  passport.use(strategy);
};

AuthUtils.useOAuth1Strategy = function(Strategy, name, options) {
  var authSuccess = function(token, tokenSecret, profile, done) {
    var data = {
      token: token,
      token_secret: tokenSecret,
      consumer_key: options.consumerKey,
      consumer_secret: options.consumerSecret
    };
    if(profile) {
      data.user_profile = profile;
    }

    done(null, data);
  };

  var strategy = new Strategy(options, authSuccess);
  passport.use(name, strategy);
};

AuthUtils.useOAuth2Strategy = function(Strategy, name, options) {
  var authSuccess = function(accessToken, refreshToken, profile, done) {
    var data = {
      access_token: accessToken,
      refresh_token: refreshToken
    };
    if(profile) {
      data.user_profile = profile;
    }

    done(null, data);
  };

  var strategy = new Strategy(options, authSuccess);
  passport.use(name, strategy);

  // OAuth2 tokens can be automatically refreshed,
  // so register this strategy with the refresh module too
  refresh.use(name, strategy);
};

AuthUtils.authorize = function(integration, credentials, callbackURL) {
  switch(integration.authType) {
    case 'oauth1':
      return AuthUtils.oauth1Authorize(integration, credentials, callbackURL);
    case 'oauth2':
      return AuthUtils.oauth2Authorize(integration, credentials, callbackURL);
  }
};

AuthUtils.oauth1Authorize = function(integration, credentials, callbackURL) {
  let defer = Q.defer();
  let oauth1  = new oauth.OAuth(
    integration.authOptions.authorizationURL,
    integration.authOptions.tokenURL,
    credentials.oauth_id,
    credentials.oauth_secret,
    '1.0',
    callbackURL,
    'HMAC-SHA1'
  );
  oauth1.getOAuthRequestToken((err, oAuthToken) => {
    if(err) {
      return defer.reject(err);
    }
    let authURL = `https://twitter.com/oauth/authenticate?oauth_token=${oAuthToken}`;
    defer.resolve(authURL);
  });
  return defer.promise;
};

AuthUtils.oauth2Authorize = function(integration, credentials, callbackURL) {
  let oauth2 = new oauth.OAuth2(
    credentials.oauth_id,
    credentials.oauth_secret,
    '',
    integration.authOptions.authorizationURL,
    integration.authOptions.tokenURL
  );
  let options = _.assign(integration.authParams, {
    redirect_uri: callbackURL
  });

  if(integration.authOptions.state) {
    // TODO
    //options.state = 'some random string to protect against cross-site request forgery attacks'
  }

  let authURL = oauth2.getAuthorizeUrl(options);
  return Q(authURL);
};

AuthUtils.requestAccessToken = function(integration, data, credentials, callbackURL) {
  switch(integration.authType) {
    case 'oauth1':
      return AuthUtils.oauth1requestAccessToken(integration, data, credentials, callbackURL);
    case 'oauth2':
      return AuthUtils.oauth2requestAccessToken(integration, data, credentials, callbackURL);
  }
};

AuthUtils.oauth1requestAccessToken = function(integration, data) {
  return Q(data);
};

AuthUtils.oauth2requestAccessToken = function(integration, data, credentials, callbackURL) {
  let defer = Q.defer();
  let query = {
    client_id: credentials.oauth_id,
    client_secret: credentials.oauth_secret,
    code: data.code,
    redirect_uri: callbackURL
  };
  let opt = {
    url: integration.authOptions.tokenURL + '?' + qs.stringify(query),
    headers: {
      Accept: 'application/json'
    },
    json: true,
    timeout: 30000
  };
  request(opt, (err, res, body) => defer.resolve(body));
  return defer.promise;
};
