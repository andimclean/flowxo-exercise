'use strict';

var _ = require('lodash'),
    HttpRequestError = requireLocal('errors/httpRequest.error');

var ErrorUtils = module.exports = {};

ErrorUtils.newError = function(options) {
  var err = new Error();
  _.assign(err, options);
  return err;
};

ErrorUtils.rewriteValidationError = function(errors, oldPath, newPath) {
  if(errors[oldPath]) {
    errors[newPath] = errors[oldPath];
    errors[newPath].path = newPath;
    errors[newPath].message =
      errors[newPath].message.replace(
        new RegExp(oldPath, 'g'), newPath);

    delete errors[oldPath];
  }
};

ErrorUtils.isInvalidCredentialsError = function(err) {
  return err && err.name === 'InvalidCredentialsError';
};

ErrorUtils.isInvalidBillingTransitionError = function(e) {
  return e instanceof HttpRequestError &&
         e.status === 400 &&
         e.err &&
         e.err.symbol === 'invalid_transition';
};

ErrorUtils.normaliseErrorMessage = function(e, fallback) {
  e = e || new Error();

  if(!e.message) {
    // Try and build message from embedded error.
    if(e.err) {
      if(e.err.message) {
        e.message = e.err.message;
      } else if(_.isFunction(e.err.toString)) {
        e.message = e.err.toString();
      }
    }
    if(!e.message) {
      // No message? Use the fallback.
      e.message = fallback;
    }
  }

  return e;
};

ErrorUtils.getErrorByStatusCode = function(code, message, err) {
  switch(code) {
    case 400:
      return HttpRequestError.new400BadRequestError(message, err);

    case 401:
      return HttpRequestError.new401UnauthorizedError(message, err);

    case 403:
      return HttpRequestError.new403ForbiddenError(message, err);

    case 404:
      return HttpRequestError.new404NotFoundError(message, err);

    case 501:
      return HttpRequestError.new501NotImplementedError(message, err);

    case 503:
      return HttpRequestError.new503ServiceUnavailableError(message, err);

    case 504:
      return HttpRequestError.new504GatewayTimeoutError(message, err);

    default:
      return HttpRequestError.new500ServerError(message, err);
  }
};

// Proxy calls to create HttpRequestErrors.
// These are here so that legacy code doesn't need
// to be updated.
ErrorUtils.get400BadRequestError = function(message, err) {
  return HttpRequestError.new400BadRequestError(message, err);
};

ErrorUtils.get401UnauthorizedError = function(message, err) {
  return HttpRequestError.new401UnauthorizedError(message, err);
};

ErrorUtils.get403ForbiddenError = function(message, err) {
  return HttpRequestError.new403ForbiddenError(message, err);
};

ErrorUtils.get404NotFoundError = function(message, err) {
  return HttpRequestError.new404NotFoundError(message, err);
};

ErrorUtils.get500ServerError = function(message, err) {
  return HttpRequestError.new500ServerError(message, err);
};

ErrorUtils.get501NotImplementedError = function(message, err) {
  return HttpRequestError.new501NotImplementedError(message, err);
};

ErrorUtils.get503ServiceUnavailableError = function(message, err) {
  return HttpRequestError.new503ServiceUnavailableError(message, err);
};

ErrorUtils.get504GatewayTimeoutError = function(message, err) {
  return HttpRequestError.new504GatewayTimeoutError(message, err);
};
