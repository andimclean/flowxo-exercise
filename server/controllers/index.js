'use strict';

var async = require('async'),
    _ = require('lodash'),
    logger = requireLocal('logger'),
    path = require('path'),
    url = require('url'),
    config = requireLocal('config');

var getViewData = function(view, cb) {
  var data = {
    config: config,
    services: []
  };

  switch(view) {
    default:
      cb(data);
      break;
  }
};

var renderView = function(res, requestedView, done) {
  if(process.env.APP_UI_DISABLED && /^(\/app)/.test(requestedView)) {
    requestedView = '/maintenance';
  }

  requestedView = path.join('.', requestedView);
  done = done || function() {};

  getViewData(requestedView, function(data) {
    // Try to render the view.
    res.render(requestedView, data, function(err, html) {
      if(err) { return done(err); }
      res.send(html);
      done();
    });
  });
};

var tryView = function(req, res, onPathNotFound) {
  // Parse out the pathname from the url, stripping
  // out any file extension.
  var pathname = url.parse(req.url).pathname;
  var view = pathname === '/' ? '/index' : pathname.split('.')[0];

  renderView(res, view, function(err) {
    if(err) {
      // console.log('error', err, view, pathname);
      onPathNotFound(err, view);
    }
  });
};

/**
 * Send partial, or 404 if it doesn't exist
 */
exports.partials = function(req, res) {
  tryView(req, res, function(err, requestedView) {
    logger.warn('Error rendering %s: %s', requestedView, err.toString());
    res.status(404).end();
  });
};

/**
 * Send a translation, or the default one (English) if it doesn't exist
 */
exports.translations = function(req, res) {
  var requestedFile = path.join('./server/', req.url);
  res.sendfile(requestedFile, function(err) {
    if(err) {
      logger.warn('Error sending translation file %s', requestedFile, err);

      // Rewrite the request to be 'en.json' instead
      res.redirect(req.path.replace(/\/[^\/]*$/, '/en.json'));
    }
  });
};

/**
 * Try and send the HTML page
 * If it doesn't exist, send the index page
 * This allows single page app in HTML5 history mode
 */
exports.app = function(req, res) {
  tryView(req, res, function() {
    // The HTML file isn't on the server.
    // This is probably a HTML5 route in the Angular app,
    // so send the index file and let Angular take over.
    renderView(res, '/app');
  });
};

/**
 * Try and send the file. If it can't be found,
 * show the 404.
 */
exports.static = function(req, res) {
  tryView(req, res, function() {
    // Render the 404 file.
    renderView(res, '/404');
  });
};
