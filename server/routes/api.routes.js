'use strict';

const express = require('express');
const mongoose = require('mongoose');

const Note = mongoose.connection.model("Note");

var router = express.Router();


router.route('/note')
  .get(function (req, res) {
    Note
      .find()
      .sort({
        updatedAt: 'desc'
      })
      .then((notes) => {
        res.send({
          result: 'ok',
          notes: notes || {}}
        );
        res.s
        res.status(200).end();
      })
      .catch(error => {
        res.send({
          result: "error",
          error: error
        });
        res.status(500).end();
      });
  })
  .post(function (req, res) {
    var note = new Note({
      text: req.body.text
    })
    note.save()
      .then(note => {
        res.send({
          result: 'ok',
          note: note || {}}
        );
        res.status(200).end();
      })
      .catch(error => {
        res.send({
          result: "error",
          error: error
        });
        res.status(500).end();
      });
  });

router.route('/note/:noteid')
  .get(function (req, res) {
    Note
      .findById(req.params.noteid)
      .then((notes) => {
        res.send({
          result: 'ok',
          note: notes || {}
        });
        res.status(200).end();
      })
      .catch(error => {
        res.send({
          result: "error",
          error: error
        });
        res.status(500).end();
      });;
  })
  .delete(function (req, res) {
    Note
      .findById(req.params.noteid)
      .remove()
      .then((_) => {

        res.send({
          result: "ok"
        });
        res.status(200).end();
      })
      .catch(error => {
        res.send({
          result: "error",
          error: error
        });
        res.status(500).end();
      });;
  })
  .patch(function (req, res) {
    Note.findByIdAndUpdate(req.params.noteid, {
        text: req.body.text
      }, {
        new: true
      })
      .then(note => {
        res.send({
          result: 'ok',
          note: note
        });
        res.status(200).end();
      })
      .catch(error => {
        res.send({
          result: "error",
          error: error
        });
        res.status(500).end();
      });
  })
  .put(function (req, res) {
    Note.findByIdAndUpdate(req.params.noteid, {
        text: req.body.text
      }, {
        new: true
      })
      .then(note => {
        res.send({
          result: 'ok',
          note: note
        });
        res.status(200).end();
      })
      .catch(error => {
        res.send({
          result: "error",
          error: error
        });
        res.status(500).end();
      });
  });

// All undefined api routes should return a 404
router.route('/*')
  .all(function (req, res) {
    res.status(404).end();
  });

module.exports = router;