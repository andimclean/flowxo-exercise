'use strict';

var index = requireLocal('controllers'),
    config = requireLocal('config'),
    routes = {
      api: require('./api.routes'),
    };

/**
 * Application routes
 */
module.exports = function(expressConfig) {

  // Only use sessions for some endpoints.
  var app = expressConfig.app;

  var publicApiMiddleware = [
    routes.api
  ];

  /**
   * The public JSON API, unprotected.
   */
  app.use(config.urls.api, publicApiMiddleware);

  /** Views all have session support */

  // Allow HTML5 mode for angular app
  app.route(config.urls.app + '/*')
    .get(index.app);

  //
  // // For everything else
  app.route('/*')
    .get(index.static);

};
