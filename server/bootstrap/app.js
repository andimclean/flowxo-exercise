'use strict';

var async = require('async');

// Setup & enable colors
var colors = require('colors');
colors.enabled = true;

var app;

module.exports = function(options, done) {
  // Prevent startup from being run more than once.
  if(!app) {
    options = options || {};

    app = {};

    var logger = requireLocal('logger');

    // Grab the config options for this environment
    var config = requireLocal('config');

    // Promises configuration
    requireLocal('config/q');

    // Mongoose configuration
    requireLocal('config/mongoose');

    // Now connect to our network services.
    async.waterfall([
      function(cb) {
        // Connect to Database.
        require('./services/mongodb')(config.mongo, cb);
      },

      function(cb) {
        require('./services/expressServer')(config, cb);
      },

      function(server, cb) {
        app.server = server;
        cb();
      }

    ], function(err) {
      if(err) {
        logger.warn('Could not start:', err);
        // Call the callback, otherwise throw the error.
        if(done) { return done(err); }
        throw err;
      }

      logger.info('Started successfully');
      if(done) { done(null, app); }
    });
  }
};
