// This file is named 'expressServer' and not just 'express'
// because newrelic barfs if there is a file named 'express'
// anywhere in the codebase.

'use strict';

const fs = require('fs');
const path = require('path');
const http = require('http');
const https = require('https');
const Q = require('q');
const express = require('express');
const logger = requireLocal('logger');
const gracefuls = requireLocal('gracefuls');

module.exports = function(config, done) {
  var app = express();

  var expressConfig = requireLocal('config/expressConfig')(app);
  requireLocal('routes')(expressConfig);

  var server;
  if(config.ssl && config.ssl.enable) {
    server = https.createServer({
      key: fs.readFileSync(path.join(rootPath, config.ssl.key)),
      cert: fs.readFileSync(path.join(rootPath, config.ssl.cert))
    }, app);
  } else {
    server = http.createServer(app);
  }

  // Start server
  server.listen(config.port, config.ip, function() {
    logger.info('Web server listening on %s:%d, in %s mode', config.ip, config.port, app.get('env'));

    gracefuls.add('express', function() {
      logger.info('Shutting down Express server...');
      return Q.Promise(function(resolve) {
        server.close(function() {
          logger.info('Express server shutdown complete.');
          resolve();
        });
      });
    });

    done(null, app);
  });
};
