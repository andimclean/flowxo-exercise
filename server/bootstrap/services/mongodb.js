'use strict';

const mongoose = require('mongoose');
const Q = require('q');
const logger = requireLocal('logger');
const gracefuls = requireLocal('gracefuls');

module.exports = function(config, done) {
  if(config.debug) {
    mongoose.set('debug', true);
  }

  mongoose.connect(config.uri, config.options, function(err) {
    if(err) {
      return done(err);
    }

    logger.info('Connected to MongoDB.');

    gracefuls.add('mongo', function() {
      return Q.Promise(function(resolve) {
        logger.info('Disconnecting from MongoDB...');
        mongoose.disconnect(function(err) {
          if(err) {
            logger.warn('Error disconnecting from MongoDB:', err);
          } else {
            logger.info('MongoDB disconnection successful.');
          }
          resolve();
        });
      });
    });

    done();
  });
};
