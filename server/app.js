'use strict';

require('./globals');


// Run in regular mode.
require('./bootstrap/app')({
  web: true,
  worker: true
});
