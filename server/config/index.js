'use strict';

var _ = require('lodash');

var env = process.env.NODE_ENV;

/**
 * Load environment configuration
 */
var config = module.exports = _.merge(
  require('./env/all.js'),
  require('./env/' + env + '.js') || {});

config.base_url_is_https =
  config.urls.base &&
  config.urls.base.slice &&
  config.urls.base.slice(0, 5) === 'https';
