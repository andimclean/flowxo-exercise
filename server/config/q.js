'use strict';

var Q = require('q');

Q.onerror = function(err) {
  // Throw the error so that the uncaughtException handler catches it above
  throw err;
};

if(/^(development|test)$/.test(process.env.NODE_ENV)) {
  Q.longStackSupport = true;
}
