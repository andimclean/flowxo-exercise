'use strict';

var express = require('express'),
    helmet = require('helmet'),
    mongoose = require('mongoose'),
    favicon = require('serve-favicon'),
    morgan = require('morgan'),
    compression = require('compression'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    cookieParser = require('cookie-parser'),
    expressValidator = require('express-validator'),
    path = require('path'),
    config = require('./index'),
    logger = requireLocal('logger');

// Add XML parsing to bodyParser
require('body-parser-xml')(bodyParser);

// Even though we are using JWTs, we still need sessions
// for external service authentication (we put the
// service connection name and service ID in the session
// before calling out to do external service auth).
var session = require('express-session'),
    MongoStore = require('connect-mongo')(session);

var sessionTTL = config.session_max_age / 1000;

var getMongoStore = function() {
  return new MongoStore({
    mongooseConnection: mongoose.connection,
    collection: 'sessions',
    ttl: sessionTTL,
    touchAfter: sessionTTL / 2,
    stringify: true
  });
};

/**
 * Express configuration
 */
module.exports = function(app) {
  var env = app.get('env');
  var sessionStore;

  if ('development' === env) {
    app.use(require('connect-livereload')());

    // Disable caching of scripts for easier testing
    app.use(function noCache(req, res, next) {
      if (req.url.indexOf('/scripts/') === 0) {
        res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
        res.header('Pragma', 'no-cache');
        res.header('Expires', 0);
      }
      next();
    });

    app.use(express.static(path.join(config.root, '.tmp')));
    app.use(express.static(path.join(config.root, 'client')));
    app.set('views', config.root + '/client/views');
    app.use(morgan('dev', {
      stream: {
        write: function(message) {
          logger.debug(message.slice(0, -1));
        }
      }
    }));

    sessionStore = getMongoStore();
  }

  if ('production' === env) {
    // Allow 'real' IP Address of client to be set in req.ip
    // http://expressjs.com/guide/behind-proxies.html
    app.set('trust proxy', true);

    // Secure the server with helmet.js
    // https://github.com/helmetjs/helmet
    // TODO content security policy?

    // Protect against cross site scripting
    app.use(helmet.xssFilter());

    // Don't allow anyone to put me in a frame.
    app.use(helmet.frameguard('deny'));

    if(config.base_url_is_https) {
      // Prefer connection over https
      var ninetyDaysInSeconds = 7776000;
      app.use(helmet.hsts({ maxAge: ninetyDaysInSeconds }));
    }

    // Prevent IE from executing downloads in app context
    app.use(helmet.ieNoOpen());

    // Don't allow mime type sniffing
    app.use(helmet.noSniff());

    // Hide the X-Powered-By header
    app.use(helmet.hidePoweredBy());

    app.use(compression());

    app.use(favicon(path.join(config.root, 'public', 'favicon.ico')));
    app.use(express.static(path.join(config.root, 'public')));
    app.set('views', config.root + '/views');

    sessionStore = getMongoStore();
  }

  if('test' === env) {
    app.set('views', config.root + '/test/server/fixtures/views');

    // Mock the sessions using an in-memory session store.
    // This prevents TTL index errors.
    // http://stackoverflow.com/a/24709289/1171775
    sessionStore = new session.MemoryStore();
  }

  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');

  // Disable etag for API endpoints only
  app.set('etag', false);

  // Allow parsing with `qs` lib to support richer query string
  // https://www.npmjs.org/package/qs#readme
  app.use(bodyParser.urlencoded({
    limit: config.requests.received_body_limit,
    extended: true
  }));

  // Accept JSON
  app.use(bodyParser.json({
    limit: config.requests.received_body_limit
  }));

  // Accept XML
  app.use(bodyParser.xml({
    limit: config.requests.received_body_limit,
    xmlParseOptions: config.xml_parse_options
  }));

  // Accept plain text
  app.use(bodyParser.text({
    limit: config.requests.received_body_limit
  }));


  app.use(expressValidator());
  app.use(methodOverride());
  app.use(cookieParser());

  return {
    app: app
  };
};
