'use strict';

// Use Q promises for mongoose
var mongoose = require('mongoose');
var Q = require('q');

var Schema = mongoose.Schema;

mongoose.Promise = Q.Promise;

var NoteSchema = Schema({text:String},{timestamps: true, collection: "notes"});
var Note = mongoose.model("Note",NoteSchema);
