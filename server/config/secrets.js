'use strict';

var requiredSecrets = [
  'SESSION_SECRET',
  'SERVICE_CONNECTION_SECRET',
  'INTEGRATION_CONNECTION_SECRET',
  'HEARTBEAT_ACCESS_TOKEN',
  'BILLING_ACCESS_TOKEN',
  'ADMIN_COMMANDS_ACCESS_TOKEN',
  'MANDRILL_EMAIL_TRIGGER_WEBHOOK_KEY',
  'MANDRILL_EMAIL_AGENT_WEBHOOK_KEY',
  'MANDRILL_EMAIL_BROADCAST_WEBHOOK_KEY',
  'AGENT_MODE_EMAIL_VERIFICATION_SECRET',
  'FACEBOOK_VERIFY_TOKEN_SECRET'
];

var available = {}, missing = [];

requiredSecrets.forEach(function(s) {
  if(!process.env[s]) {
    missing.push(s);
  } else {
    available[s] = process.env[s];
  }
});

exports.available = available;
exports.missing = missing;
