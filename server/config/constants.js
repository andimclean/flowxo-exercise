'use strict';

///////////////////////////////////////////////////////////////
// Constants are all functions, to ensure that               //
// each time they are requested, a fresh object is returned. //
// This eliminates the possibility of mistakingly            //
// overwriting constants used elsewhere.                     //
///////////////////////////////////////////////////////////////
var Constants = {};

Constants.taskKinds = function() {
  return ['trigger', 'task'];
};

Constants.taskTypes = function() {
  return [
    'service',
    'webhook',
    'email',
    'wait',
    'integration',
    'integration_poller',
    'integration_webhook',
    'flow'
  ];
};

Constants.fieldTypes = function() {
  return [
    // inputs
    'text',
    'password',
    'email',
    'select',
    'textarea',
    'datetime',
    'boolean',
    'dictionary',
    'list',
    'hidden',
    'code',

    // outputs
    'collection',
    'data'
  ];
};

Constants.taskFilterOps = function() {
  return [
    'exists',
    'not_exists',
    'equals',
    'not_equals',
    'contains',
    'not_contains',
    'equals_one_of',
    'not_equals_one_of',
    'starts_with',
    'not_starts_with',
    'ends_with',
    'not_ends_with',
    'greater_than',
    'greater_than_equals',
    'less_than',
    'less_than_equals'
  ];
};

Constants.servicesAuthTypes = function() {
  return [
    'credentials',
    'oauth1',
    'oauth2'
  ];
};

Constants.integrationsAuthTypes = function() {
  return [
    'credentials',
    'oauth1',
    'oauth2',
    'link'
  ];
};

module.exports = Constants;
