// Billing plans.

'use strict';

var _ = require('lodash');

var processPlan = function(plan, idx) {
  plan.trigger_poller_interval_ms = plan.trigger_poller_interval_mins * 60000;
  plan.rank = idx + 1;
  if(plan.addons) {
    plan.addonsHash = _.keyBy(plan.addons, 'id');
  }
};

var plans = [{
  id: 'free',
  rateable_items_limit: 5,
  rateable_runs_limit: 500,
  trigger_poller_interval_mins: 5,
  task_run_logs_retention: '2 week',
  price: 0,
  available: true,
  aliases: ['foc']
}, {
  id: 'standard',
  rateable_items_limit: 15,
  rateable_runs_limit: 5000,
  trigger_poller_interval_mins: 1,
  task_run_logs_retention: '3 month',
  price: 19,
  available: true,
  addons: [{
    id: 'bot',
    quantity: 5,
    price: 10,
    rateable: 'item',
    available: true
  }, {
    id: 'interaction',
    quantity: 25000,
    price: 25,
    rateable: 'run',
    available: true
  }]
}, {
  id: 'micro',
  active_workflows_limit: 5,
  task_runs_limit: 5000,
  trigger_poller_interval_mins: 5,
  task_run_logs_retention: '2 week',
  price: 19,
  available: false
}, {
  id: 'plus',
  active_workflows_limit: 10,
  task_runs_limit: 15000,
  trigger_poller_interval_mins: 1,
  task_run_logs_retention: '6 month',
  price: 49,
  available: false
}, {
  id: 'pro',
  active_workflows_limit: 20,
  task_runs_limit: 45000,
  trigger_poller_interval_mins: 1,
  task_run_logs_retention: '1 year',
  price: 99,
  available: false
}];

plans.forEach(processPlan);

exports.all = plans;
exports.all_hash = _.keyBy(exports.all, 'id');
exports.available = _.filter(plans, 'available');
exports.available_hash = _.keyBy(exports.available, 'id');
exports.default = plans[0];
