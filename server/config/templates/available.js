'use strict';

// Templates are shared flows which are presented
// for installation in a prominant location.
// Icons are available in the `icons/templates`
// folder in the root of the project.
//
// This file is used by the Gruntfile to generate
// the icons, so should be standalone.

module.exports = [{
  key: 'blank',
  name: 'Blank Flow',
  theme: '#fff'
}, {
  key: 'book_a_room',
  env: 'FLOW_TEMPLATE_BOOK_A_ROOM',
  name: 'Book a Room',
  theme: '#cdf4d7'
}, {
  key: 'book_a_table',
  env: 'FLOW_TEMPLATE_BOOK_A_TABLE',
  name: 'Book a Table',
  theme: '#00b2cc'
}, {
  key: 'book_a_service',
  env: 'FLOW_TEMPLATE_BOOK_A_SERVICE',
  name: 'Book a Service',
  theme: '#6f5fe8'
}, {
  key: 'buy_tickets',
  env: 'FLOW_TEMPLATE_BUY_TICKETS',
  name: 'Buy Tickets',
  theme: '#e6606b'
}, {
  key: 'apply',
  env: 'FLOW_TEMPLATE_APPLY',
  name: 'Apply or Sign Up',
  theme: '#f37a71'
}, {
  key: 'qa',
  env: 'FLOW_TEMPLATE_QA',
  name: 'FAQ',
  theme: '#323a46'
}, {
  key: 'talk_to_a_human',
  env: 'FLOW_TEMPLATE_TALK_TO_A_HUMAN',
  name: 'Message a Human',
  theme: '#b6e8fa'
}, {
  key: 'small_talk',
  env: 'FLOW_TEMPLATE_SMALL_TALK',
  name: 'Small Talk',
  theme: '#67c18e'
}, {
  key: 'welcome',
  env: 'FLOW_TEMPLATE_WELCOME',
  name: 'Welcome & Help',
  theme: '#323a46'
}, {
  key: 'find_location',
  env: 'FLOW_TEMPLATE_FIND_LOCATION',
  name: 'Find Location',
  theme: '#b6c1fa'
}, {
  key: 'news_feed',
  env: 'FLOW_TEMPLATE_NEWS_FEED',
  name: 'News Feed',
  theme: '#cdf4d7'
}];
