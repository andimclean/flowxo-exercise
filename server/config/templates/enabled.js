'use strict';

const iconsProfile = require('../icons/profile');
const available = require('./available');

module.exports = function(baseUrl) {
  return available.reduce((acc, item) => {
    let configured;

    if(item.key === 'blank') {
      configured = {
        blank: true
      };
    } else if(item.env && process.env[item.env]) {
      configured = {
        _id: process.env[item.env]
      };
    }

    if(configured) {
      const { key, name, theme } = item;
      const iconProfile = iconsProfile(baseUrl, 'template-icons', item.key);
      acc.push(Object.assign(configured, iconProfile, {
        key, name, theme
      }));
    }
    return acc;
  }, []);
};
