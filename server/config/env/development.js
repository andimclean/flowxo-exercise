'use strict';

var config = {
  env: 'development',
  server_label: 'Development',
  port: 9000,

  mongo: {
    debug: +process.env.MONGO_DEBUG,
    uri: process.env.MONGO_URL ||
         'mongodb://localhost/flowxo-dev'
  },

};

module.exports = config;
