/* globals jasmine */

'use strict';

module.exports = {
  env: 'test',
  urls: {
    base: '',
    botmaster_url: ''
  },
  mongo: {
    uri: process.env.MONGO_TEST_URL || 'mongodb://localhost/flowxo-test',
    max_ops_per_bulk: 3
  }
};
