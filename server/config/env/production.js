'use strict';

var path = require('path');

var hasEnv = function(key) {
  return process.env.hasOwnProperty(key);
};

var getQueueCapacity = function(env, fallback) {
  if(!hasEnv('WEB_MEMORY') || !hasEnv(env)) {
    return fallback;
  }

  var ratio = Math.floor(process.env.WEB_MEMORY / process.env[env]);
  if(Number.isNaN(ratio)) {
    return fallback;
  }

  return ratio > 1 ? ratio : 1;
};

module.exports = {
  env: 'production',
  ip:   process.env.IP || '0.0.0.0',
  port: process.env.PORT || 8080,
  services: {
    icons_root_path: path.join(rootPath, 'public', 'images', 'service-icons'),
  },
  mongo: {
    debug: process.env.MONGO_DEBUG_ENABLE,
    uri: process.env.MONGO_URL ||
         'mongodb://localhost/flowxo'
  },
  redis: {
    uri: process.env.REDIS_URL ||
         'redis://localhost:6379/1'
  },
  logs: {
    Console: {
      timestamp: false
    }
  },
  mq: {
    queues: {
      workflow: {
        capacity: getQueueCapacity('JOB_MEMORY', 2),
        instrument: true,
        log_capacity_reports: true
      }
    }
  },
  webhooks: {
    integration_created: process.env.WEBHOOK_INTEGRATION_CREATED,
    subscription_changed: process.env.WEBHOOK_SUBSCRIPTION_CHANGED,
    user_created: process.env.WEBHOOK_USER_CREATED,
    user_limits_exceeded: process.env.WEBHOOK_USER_LIMITS_EXCEEDED,
    user_runs_threshold_passed: process.env.WEBHOOK_USER_RUNS_THRESHOLD_PASSED,
    user_signed_in: process.env.WEBHOOK_USER_SIGNED_IN,
    workflow_activated: process.env.WEBHOOK_WORKFLOW_ACTIVATED,
    workflow_created: process.env.WEBHOOK_WORKFLOW_CREATED,
    workflow_shared: process.env.WEBHOOK_WORKFLOW_SHARED,
    workflow_task_created: process.env.WEBHOOK_WORKFLOW_TASK_CREATED
  },
  auth: {
    loader_options: {
      print_stack_trace_on_error: true
    }
  },
  email: {
    authenticate_mandrill_hook: true
  },
  secrets: require('../secrets').available,
  report_errors: true,
  graceful_shutdown: true,
  allow_timezone_lookup: true,
  newrelic: {
    agent_enabled: true
  }
};
