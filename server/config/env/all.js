'use strict';

module.exports = {
  ip: 'localhost',
  port: process.env.PORT || 9000,
  // session_ttl should be at least 24 hours to support referrals
  session_max_age: 1000 * 60 * 60 * 36,
  root: rootPath,
  urls: {
    base: process.env.BASE_URL,
    api: '/api',
    app: '/app',
  },
  requests: {
    received_body_limit: '3MB'
  },
  mongo: {
    max_ops_per_bulk: 1000,
    options: {
      db: {
        safe: true
      }
    }
  }
};
