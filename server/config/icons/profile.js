'use strict';

const definitions = require('./definition');

module.exports = function(baseUrl, type, name) {
  const iconsBaseUrl = `${baseUrl}/images/${type}/${name}`;
  return {
    icons_base_url: iconsBaseUrl,
    icons_available_widths: definitions.available_widths,
    icons_available_pixel_ratios: definitions.available_pixel_ratios,
    icon_url: `${iconsBaseUrl}/icon.png`
  };
};
