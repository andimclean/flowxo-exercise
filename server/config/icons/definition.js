'use strict';

module.exports = {
  available_widths: [50, 100, 200, 300],
  available_pixel_ratios: [1, 2, 3],
  default_width: 200
};
