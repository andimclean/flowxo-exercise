'use strict';

module.exports = {
  facebook: {
    type: 'oauth2',
    strategy: require('passport-facebook').Strategy,
    options: {
      clientID: process.env.FACEBOOK_ID,
      clientSecret: process.env.FACEBOOK_SECRET,
      enableProof: true,
      state: true
    },
    params: {
      scope: ['public_profile', 'email']
    }
  },
  google: {
    type: 'oauth2',
    strategy: require('passport-google-oauth20').Strategy,
    options: {
      clientID: process.env.GOOGLE_ID,
      clientSecret: process.env.GOOGLE_SECRET,
      state: true
    },
    params: {
      scope: ['profile', 'email'],
      prompt: 'select_account'
    }
  },
  linkedin: {
    type: 'oauth2',
    strategy: require('passport-linkedin-oauth2').Strategy,
    options: {
      clientID: process.env.LINKEDIN_ID,
      clientSecret: process.env.LINKEDIN_SECRET,
      state: true,
      profileFields: [
        'id',
        'first-name',
        'last-name',
        'picture-url',
        'formatted-name',
        'email-address',
      ]
    },
    params: {
      scope: ['r_emailaddress', 'r_basicprofile']
    }
  },
  local: {
    type: 'local',
    strategy: require('passport-local'),
    options: {
      usernameField: 'email',
      session: false,
      passReqToCallback: true
    }
  }
};
