'use strict';

// Graceful shutdown handler
const Q = require('q');
const config = requireLocal('config');
const logger = requireLocal('logger');

class GracefulShutdown {
  constructor() {
    this.gracefuls = new Map();
    this.errorReporter = null;

    this._isShuttingDown = false;
    this._setupProcessListeners();
  }

  add(name, handler) {
    this.gracefuls.set(name, handler);
  }

  setErrorReporter(reporter) {
    this.errorReporter = reporter;
  }

  /** Private methods */

  _setupProcessListeners() {
    process.on('uncaughtException', err => this._reportErrorAndShutdown(err));
    ['SIGINT', 'SIGTERM'].forEach(sig => {
      process.on(sig, () => this._logSignalAndShutdown(sig));
    });
  }

  _reportErrorAndShutdown(err) {
    const loggable = err.stack || err;
    logger.error('Process received an uncaught exception: ', loggable);
    this._performShutdown(1, this.errorReporter && this.errorReporter(err));
  }

  _logSignalAndShutdown(sig) {
    logger.info(`Process received ${sig}`);
    this._performShutdown(0);
  }

  _performShutdown(exitCode, additional) {
    if(!this._isShuttingDown) {
      logger.debug('Performing shutdown');
      this._isShuttingDown = true;

      const promises = [ this._runGracefuls() ];
      if(additional) {
        promises.push(additional);
      }
      Q.allSettled(promises).done(() => {
        logger.info(`Process now exiting with code ${exitCode}`);
        process.exit(exitCode);
      });
    }
  }

  _runGracefuls() {
    return Q.async(function* () {
      if(!config.graceful_shutdown) {
        logger.debug('Graceful shutdown disabled by config.');
        return;
      }

      // Some shutdown happens first, then we do the rest.
      // Needs to all happen within 10 secs.
      const firstWave = [];
      this.gracefuls.forEach((val, key) => {
        if(key === 'express' || key.startsWith('queue-')) {
          this.gracefuls.delete(key);
          firstWave.push(this._handleGraceful(val, 7000));
        }
      });
      yield Q.allSettled(firstWave);

      const secondWave = [];
      this.gracefuls.forEach((val, key) => {
        this.gracefuls.delete(key);
        secondWave.push(this._handleGraceful(val, 1000));
      });
      yield Q.allSettled(secondWave);

      // Return a success, always.
      return true;

    }).call(this);
  }

  _handleGraceful(handler, timeout) {
    // Always resolve the promise, whatever happens.
    return Q.Promise(function(resolve) {
      handler().timeout(timeout).done(resolve, resolve);
    });
  }
}

module.exports = new GracefulShutdown();
