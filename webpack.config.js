'use strict';

const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const path = require('path');

let config = {
  output: {
    path:  path.resolve(__dirname, '.tmp', 'scripts'),
    publicPath: '/scripts/',
    filename: '[name].js'
  },
  resolve: {
    modules: [
      'node_modules'
    ],
    alias: {
      hljs: path.resolve(__dirname, 'node_modules', 'highlight.js', 'lib', 'highlight.js')
    }
  }
};

if(process.env.NODE_ENV === 'production') {
  config = webpackMerge(config, {
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': '"production"'
      }),
      new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false
      }),
      new webpack.optimize.UglifyJsPlugin({
        beautify: false,
        mangle: {
          screw_ie8: true,
          keep_fnames: true
        },
        compress: {
          warnings: false,
          screw_ie8: true
        },
        comments: false,
        sourceMap: false
      })
    ]
  });

} else {
  config = webpackMerge(config, {
    devtool: 'source-map',
    plugins: [
      new webpack.LoaderOptionsPlugin({
        debug: true
      })
    ]
  });
}

module.exports = config;