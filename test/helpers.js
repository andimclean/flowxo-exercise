/* globals jasmine, define, beforeEach */

'use strict';

// Import lodash: support both node and browser
(function (root, factory) {

    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['lodash'], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('lodash'));
    } else {
        // Browser globals (root is window)
        root.returnExports = factory(root._);
    }
}(this, function (_) {

  // Custom matchers
  beforeEach(function() {
    jasmine.addMatchers({

      toExist: function() {
        return {
          compare: function(actual) {
            return {
              pass: !_.isUndefined(actual) && !_.isNull(actual)
            };
          }
        };
      },

      toExtend: function() {
        return {
          compare: function(Actual, Expected) {
            return {
              pass: _.isFunction(Actual) && new Actual() instanceof Expected
            };
          }
        };
      },

      toBeWithShortMessage: function() {
        return {
          compare: function(actual, expected) {
            if(actual === expected) {
              return {
                pass: true,
              };
            }

            return {
              pass: false,
              // Use the standard `toString` method instead of unfolding the whole object
              message: 'Expected ' + actual + ' to be ' + expected,
            };
          },
        };
      },

      toBeArray: function() {
        return {
          compare: function(actual) {
            return {
              pass: _.isArray(actual)
            };
          }
        };
      },

      toBeBoolean: function() {
        return {
          compare: function(actual) {
            return {
              pass: _.isBoolean(actual)
            };
          }
        };
      },

      toBeDate: function() {
        return {
          compare: function(actual) {
            return {
              pass: _.isDate(actual)
            };
          }
        };
      },

      toDeepEqual: function() {
        return {
          compare: function(actual, expected) {
            return {
              pass: _.isEqual(actual, expected)
            };
          }
        };
      },

      toBeEmpty: function() {
        return {
          compare: function(actual) {
            return {
              pass: _.isEmpty(actual)
            };
          }
        };
      },

      toBeFinite: function() {
        return {
          compare: function(actual) {
            return {
              pass: _.isFinite(actual)
            };
          }
        };
      },

      toBeFunction: function() {
        return {
          compare: function(actual) {
            return {
              pass: _.isFunction(actual)
            };
          }
        };
      },

      toBeBetween: function() {
        return {
          compare: function(actual, min, max) {
            return {
              pass: min <= actual <= max
            };
          }
        };
      },

      toBeNaN: function() {
        return {
          compare: function(actual) {
            return {
              pass: _.isNaN(actual)
            };
          }
        };
      },

      toBeNumber: function() {
        return {
          compare: function(actual) {
            return {
              pass: _.isNumber(actual)
            };
          }
        };
      },

      toBePromise: function() {
        return {
          compare: function(actual) {
            return {
              pass:
                actual &&
                _.isFunction(actual.then) &&
                _.isFunction(actual.catch) &&
                _.isFunction(actual.finally)
            };
          }
        };
      },

      toBeRegExp: function() {
        return {
          compare: function(actual) {
            return {
              pass: _.isRegExp(actual)
            };
          }
        };
      },

      toBePlainObject: function() {
        return {
          compare: function(actual) {
            return {
              pass: _.isPlainObject(actual)
            };
          }
        };
      },

      toBeString: function() {
        return {
          compare: function(actual) {
            return {
              pass: _.isString(actual)
            };
          }
        };
      },

      toBeInstanceof: function() {
        return {
          compare: function(actual, expected) {
            return {
              pass: actual instanceof expected
            };
          }
        };
      },

      toHaveProperty: function() {
        return {
          compare: function(object, property) {
            // Cast to an object here to avoid less accurate errors
            return {
              pass: property in Object(object)
            };
          }
        };
      },

      toHaveOwnProperty: function() {
        return {
          compare: function(object, property) {
            // Cast to an object here to avoid less accurate errors
            return {
              pass: Object(object).hasOwnProperty(property)
            };
          }
        };
      },

      toMatchProperties: function(util) {
        return {
          compare: function(actual, expected, keys) {
            // Picks the values from the actual object present in the expected object, then does a comparison
            keys = keys || _.keys(expected);
            return {
              pass: util.equals(_.pick(actual, keys), _.pick(expected, keys))
            };
          }
        };
      },

      toEndWith: function() {
        return {
          compare: function(str, suffix) {
            return {
              pass: _.isString(str) && str.endsWith(suffix)
            };
          }
        };
      },

      toStartWith: function() {
        return {
          compare: function(str, prefix) {
            return {
              pass: _.isString(str) && str.startsWith(prefix)
            };
          }
        };
      },

      toInclude: function() {
        return {
          compare: function(actual, include) {
            return {
              pass: _.includes(actual, include)
            };
          }
        };
      }


      /*,

      toHaveFocus: function() {
        return {
          compare: function(actual, expected) {
            return {
              pass: actual instanceof expected
            };
          }
        };
        this.message = function(){
            return 'Expected \'' + angular.mock.dump(this.actual) + '\' to have focus';
        };

        return document.activeElement === this.actual[0];
      }*/

    });
  });

  // Ensure clock is restored after each spec
  afterEach(function() {
    jasmine.clock().uninstall();
  });
}));
