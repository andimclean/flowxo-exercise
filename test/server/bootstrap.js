// The bootstrapper is placed in its own folder.
// Previously, the Gruntfile loaded `server.js` in the root
// of the project. Jasmine then used the containing folder,
// i.e. the project root to search for jasmine helpers.
// This recursively searched all of bower_components
// and node_modules, looking for helpers.
// Obviously not ideal.
// The 'fix' is to bootstrap here, requiring the root `server.js`
// module, and Jasmine will only recursively search this folder
// for helpers - much better.

'use strict';

// Require nock, and allow all connections to localhost,
// so that our controllers can connect to the API
require('nock').enableNetConnect('127.0.0.1');

// Then, start the app
require('../../server/globals');
var server;

var appOptions = {
  web: true,      // Start the express server
  worker: false   // Don't consume the queue
};

require('../../server/bootstrap/app')(appOptions, function(err, app) {
  if(err) { throw err; }
  server = app.server;
});

beforeEach(function(done) {
  var self = this;

  // Don't begin the test until the app is running
  var checkRunning = function() {
    if(server) {
      self.app = server;
      done();
    } else {
      setTimeout(checkRunning, 100);
    }
  };

  checkRunning();
});
