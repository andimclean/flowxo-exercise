'use strict';

describe('Controller: AppCtrl', function () {

  // load the controller's module
  beforeEach(angular.mock.module('flowxo-core'));

  var ctrl,
    $scope,
    initController;

  // Grab dependencies from injector
  beforeEach(inject(function($controller, $rootScope) {
    $scope = $rootScope.$new();
    initController = function() {
      ctrl = $controller('AppCtrl', {
        $scope: $scope
      });
    };
  }));

  it('should format a date', function() {
    initController();

    expect($scope.formatDate("2018-01-01"))
      .toBe("2018-01-01 00:00:00");
  });

  it('should return the same string when <= 20 characters' , function(){
    initController();
    
    expect($scope.makeSummary("")).toEqual("");
    expect($scope.makeSummary("1")).toEqual("1");
    expect($scope.makeSummary("12345678901234567890")).toEqual("12345678901234567890");
    expect($scope.makeSummary("1234567890123456789012345678901234567890")).toEqual("12345678901234567890...");
  });

});
